import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import neighbors, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import linear_model

#load dữ liệu từ file data.csv
data = pd.read_csv(r'data.csv', encoding='utf-8', sep=';')
#thêm vào data cột đầu giá trị 1
one = np.ones( (data.shape[0], 1) )
data.insert(loc=0, column='A', value = one)
#gán data_x = 2 cột đầu của data
data_x = data[ ["A","Height"] ] 
#gán data_y = 1 cột sau của data
data_y = data["Weight"]
#Tách training và test sets
X_train,X_test,Y_train,Y_test = train_test_split(data_x, data_y, test_size = 5)


#Cài đặt LR 
regr = linear_model.LinearRegression(fit_intercept = False) #fit_intercept = false for calculating the bias
#Tạo vector w với tập training
regr.fit(X_train,Y_train)
#Dự đoán Y_pred từ X_test với model regr vừa train
Y_pred = regr.predict(X_test)

#plt.scatter(data.Height, data.Weight)
plt.plot(data.Height, data.Weight, 'ro-' )
#plt.plot(data.Height, data.Weight, color='black')
#Vẽ biểu đồ thể hiện X-test và Y_pred
plt.plot(X_test.Height, Y_pred , color='blue')
w_0 = regr.coef_[0]
w_1 = regr.coef_[1]
x0 = np.linspace(145, 185, 2)
y0 = w0 + w_1*x0
plt.plot(x0, y0, color='pink')
plt.show()