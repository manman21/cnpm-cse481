package com.eglife.base.android;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatEditText;

public class BaseEditText extends AppCompatEditText {
    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
