package com.eglife.base.services.api;

import com.google.gson.JsonElement;

public class ApiResponse<T extends BaseResponse> {
    private static final int STATUS_SUCCESS = 1;
    private static final int STATUS_ERROR = 0;
    private static final int STATUS_DATA_EMPTY = 2;


    public ApiResponse() {
    }

    public ApiResponse(String errorMessage) {
        this.status = STATUS_ERROR;
        this.message = errorMessage;
    }

    public ApiResponse(T data) {
        this.status = STATUS_SUCCESS;
        this.data = data;
    }

    public ApiResponse(JsonElement data) {
        this.status = STATUS_SUCCESS;
        this.rawData = data;
    }

    public static ApiResponse createDataEmptyResponse(String message) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.status = STATUS_DATA_EMPTY;
        apiResponse.setMessage(message);

        return apiResponse;
    }

    private int status;
    private String message;
    private JsonElement rawData;
    private T data;

    public boolean isSuccess() {
        // If response don't have status field or status is SUCCESS -> Return success
        return status == STATUS_SUCCESS;
    }

    public boolean isError() {
        return !isSuccess();
    }

    public boolean isEmptyData() {
        return status == STATUS_DATA_EMPTY;
    }

    public void setSuccessStatus() {
        status = STATUS_SUCCESS;
    }

    public void setErrorStatus() {
        status = STATUS_ERROR;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonElement getRawData() {
        return rawData;
    }

    public void setRawData(JsonElement rawData) {
        this.rawData = rawData;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", rawData=" + rawData +
                ", data=" + data +
                '}';
    }
}
