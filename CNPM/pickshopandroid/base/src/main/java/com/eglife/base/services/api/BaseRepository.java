package com.eglife.base.services.api;

import android.util.Log;

import androidx.annotation.NonNull;

import com.eglife.base.component.BaseActivity;
import com.eglife.base.util.JsonUtility;
import com.google.gson.JsonElement;

import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseRepository {
    private static final String ERROR_NETWORK_MESSAGE = "Có lỗi trong quá trình kết nối đến server";
    private static final String ERROR_DATA_MESSAGE = "Có lỗi trong quá trình xử lý dữ liệu";

    private BaseActivity mHostActivity;

    public BaseRepository() {
    }

    public BaseRepository(BaseActivity hostActivity) {
        mHostActivity = hostActivity;
    }

    protected <T extends BaseResponse> void callApiWithDataResponse(Call<T> call, ApiCallBack callBack) {
        callApiCoreGetData(call, callBack, null);
    }


    protected <T extends BaseResponse> Observable<ApiResponse> callApiWithDataResponse(Call<T> call) {
        return Observable.<ApiResponse>create(emitter -> {
            callApiCoreGetData(call, null, emitter);
        });
    }


    protected void callApiWithJsonResponse(Call<JsonElement> call, ApiCallBack callBack) {
        callApiCoreGetJsonData(call, callBack, null);
    }


    protected Observable<ApiResponse> callApiWithJsonResponse(Call<JsonElement> call) {
        return Observable.<ApiResponse>create(emitter -> {
            callApiCoreGetJsonData(call, null, emitter);
        });
    }

    private <T extends BaseResponse> void callApiCoreGetData(Call<T> call, ApiCallBack callBack, ObservableEmitter<ApiResponse> emitter) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull final Response<T> response) {
                if (response.body() != null) {
                    Log.e("body","body"+response.body());
                    T result = response.body();
                    if (result.isSuccess()) {
                        exposeResponse(callBack, emitter, new ApiResponse<T>(result));
                    } else {
                        String message = result.getMessage();
                        exposeResponse(callBack, emitter, new ApiResponse(message != null ? message : ERROR_DATA_MESSAGE));
                    }

                } else {
                    exposeResponse(callBack, emitter, new ApiResponse(ERROR_DATA_MESSAGE));
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull final Throwable t) {
                Log.e("test","test"+t.getMessage());
                exposeResponse(callBack, emitter, new ApiResponse(ERROR_NETWORK_MESSAGE));
            }
        });
    }

    private void callApiCoreGetJsonData(Call<JsonElement> call, ApiCallBack callBack, ObservableEmitter<ApiResponse> emitter) {
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull final Response<JsonElement> response) {
                if (response.body() != null) {
                    JsonElement jsonElement = response.body();

                    // Check status
                    Integer status = JsonUtility.getIntValueFromJsonElement(jsonElement, "status");
                    if (status == null || status != BaseResponse.STATUS_SUCCESS) {
                        String message = JsonUtility.getStringValueFromJsonElement(jsonElement, "message");
                        exposeResponse(callBack, null, new ApiResponse(message));
                        return;
                    }

                    exposeResponse(callBack, null, new ApiResponse(jsonElement));

                } else {
                    exposeResponse(callBack, null, new ApiResponse(ERROR_DATA_MESSAGE));
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull final Throwable t) {
                exposeResponse(callBack, null, new ApiResponse(ERROR_NETWORK_MESSAGE));
            }
        });
    }


    private void exposeResponse(ApiCallBack callBack, ObservableEmitter<ApiResponse> emitter, ApiResponse apiResponse) {
        // Not set activity => Call callback immediately
        if (mHostActivity == null) {
            startExposeResponse(callBack, emitter, apiResponse);
        } else if (!mHostActivity.isRunning()) {
            // Activity is sleep => Wait activity awake
            // Wait util activity resume
            Timer checkActivityResumeTimer = new Timer();
            checkActivityResumeTimer.schedule(new TimerTask() {
                @Override
                public void run() {

                    if (mHostActivity.isRunning()) {
                        startExposeResponse(callBack, emitter, apiResponse);
                        this.cancel();
                    }

                }
            }, 100, 500);

        } else {
            // Activity is active => Call callback immediately
            startExposeResponse(callBack, emitter, apiResponse);
        }
    }

    private void startExposeResponse(ApiCallBack callBack, ObservableEmitter<ApiResponse> emitter, ApiResponse apiResponse) {
        if (callBack != null) {
            callBack.onComplete(apiResponse);
        }

        if (emitter != null) {
            emitter.onNext(apiResponse);
        }
    }

}
