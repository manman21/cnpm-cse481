package com.eglife.base.util;

import android.util.Log;

public class LoggerManager {
    public static void debug(String message) {
        Log.d("iii", message);
    }

    public static void debug(String tag, String message) {
        Log.d(tag, message);
    }

    public static void info(String message) {
        Log.i("iii", message);
    }

    public static void info(String tag, String message) {
        Log.i(tag, message);
    }

    public static void warning(String message) {
        Log.w("iii", message);
    }

    public static void warning(String tag, String message) {
        Log.w(tag, message);
    }

    public static void error(String message) {
        Log.e("iii", message);
    }

    public static void error(String tag, String message) {
        Log.e(tag, message);
    }
}
