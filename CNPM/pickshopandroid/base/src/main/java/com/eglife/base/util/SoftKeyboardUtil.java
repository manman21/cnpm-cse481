package com.eglife.base.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

public class SoftKeyboardUtil {

    public static void showSoftKeyBoard(Context context) {
        changeSoftKeyBoardState(context, true);
    }

    public static void hideSoftKeyBoard(Context context) {
        changeSoftKeyBoardState(context, false);
    }

    private static void changeSoftKeyBoardState(Context context, boolean isShow) {
        View view = ((AppCompatActivity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (isShow) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            } else {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
