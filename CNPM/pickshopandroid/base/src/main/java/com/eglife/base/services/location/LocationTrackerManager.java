package com.eglife.base.services.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;


import com.eglife.base.services.location.model.GGCheckModel;
import com.eglife.base.services.location.model.LocationModelResult;
import com.eglife.base.services.location.model.LocationOnCheckModel;
import com.eglife.base.services.location.model.MessageModel;
import com.eglife.base.services.location.model.PermissionCheckModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

public class LocationTrackerManager {
    private Activity mActivity;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;

    private boolean isRequestingLocationUpdate = false;

    private Location mCurrentLocation;
    private Subject<MessageModel> mResultCodePublisher = ReplaySubject.create(); // Publish result code from onActivityResult, onPermissionRequest
    private Subject<Location> mLocationPublisher = ReplaySubject.create(); // Publish location only
    private Subject<LocationModelResult> mLocationResultPublisher = ReplaySubject.create(); // Public LocationModel result of LocationTrackerManager class

    private long mTimeRequestInterval = 10000;
    private float mMinDistanceRequest= 0;

    public LocationTrackerManager(Activity activity) {
        mActivity = activity;
    }

    /**
     * Setup LocationModel
     */
    public void setup() {
        // Check google play service available, LocationModel permission, location turn on
        // Then start location update service
        mCheckGGServiceObservable
                .flatMap(result -> {
                    if (result) {
                        return mCheckLocationPermissionObservable;
                    } else {
                        LocationModelResult locationModelResult = new LocationModelResult(true,
                                LocationModelResult.ERROR_GG_PLAY_SERVICE_CANT_UPDATE);
                        mLocationResultPublisher.onNext(locationModelResult);
                        return Observable.empty();
                    }
                })

                .flatMap(result -> {
                    if (result) {
                        return mCheckLocationServiceOnObservable;
                    } else {
                        LocationModelResult locationModelResult = new LocationModelResult(true,
                                LocationModelResult.ERROR_REQUEST_LOCATION_PERMISSION);
                        mLocationResultPublisher.onNext(locationModelResult);
                        return Observable.empty();
                    }
                })

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe(result -> {
                    if (!result) {
                        LocationModelResult locationModelResult = new LocationModelResult(true,
                                LocationModelResult.ERROR_TURN_ON_LOCATION);
                        mLocationResultPublisher.onNext(locationModelResult);
                    } else {
                        setupDone();
                    }
                });

        // Subscribe for location update
        mLocationPublisher
                .subscribe(location -> {
                    mLocationResultPublisher.onNext(new LocationModelResult(location));
                });
    }

    /**
     * Get LocationModel Observable
     * @return
     */
    public Subject<LocationModelResult> getLocation() {
        return mLocationResultPublisher;
    }

    // =====================================================================================================================================

   private void setupDone() {
        // Init LocationModel Service
       initLocationService();

       // Publish latest location
       Location location = getLatestLocationFromPreference();
       if (location != null) {
           //mCurrentLocation = location;
           //mLocationPublisher.onNext(location);
       }

       // Get last know location
       requestLastKnowLocation();

       // Start request location update
       startLocationUpdates();
   }

    private void initLocationService() {
        // Init fused location client
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mActivity);
    }

    @SuppressLint("MissingPermission")
    private void requestLastKnowLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(mActivity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            mCurrentLocation = location;
                            saveLatestLocationFromPreference(location);
                            mLocationPublisher.onNext(location);
                        }
                    }
                });
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                // Get first location
                if (location != null) {
                    mCurrentLocation = location;
                    saveLatestLocationFromPreference(location);
                    mLocationPublisher.onNext(location);
                }
            }
        }
    };

    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {
        if (mFusedLocationClient != null && !isRequestingLocationUpdate) {
            isRequestingLocationUpdate = true;
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }

    }

    public void stopLocationUpdates() {
        if (mFusedLocationClient != null && mLocationCallback != null && isRequestingLocationUpdate) {
            isRequestingLocationUpdate = false;
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }


    // =====================================================================================================================================


    // =====================================================================================================================================
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case PLAY_SERVICES_RESOLUTION_REQUEST:
                if (isGooglePlayServiceAvailable(false)) {
                    // Can resolve google play service issue
                    mResultCodePublisher.onNext(new GGCheckModel(true));
                } else {
                    // Can't resolve google play service issue
                    mResultCodePublisher.onNext(new GGCheckModel(false));
                }
                break;

            case REQUEST_CHECK_SETTINGS:
                if (resultCode == Activity.RESULT_OK) {
                    mResultCodePublisher.onNext(new LocationOnCheckModel(true));
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    mResultCodePublisher.onNext(new LocationOnCheckModel(false));
                }
                break;

            default:
                break;

        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_LOCATION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    mResultCodePublisher.onNext(new PermissionCheckModel(true));

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    mResultCodePublisher.onNext(new PermissionCheckModel(false));
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    // =====================================================================================================================================

    // Check Google PlayService exist ===================================================================================================
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9100;

    private Observable<Boolean> mCheckGGServiceObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
        @Override
        public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
            if (!isGooglePlayServiceAvailable(true)) {
                // Can't resolve google play service error
                e.onNext(false);
            } else if (isGooglePlayServiceAvailable(false)) {
                e.onNext(true);
            } else {
                mResultCodePublisher.subscribe(result -> {
                    if (result instanceof GGCheckModel) {
                        e.onNext(((GGCheckModel) result).isErrorResolved());
                    }
                });
            }
        }
    });

    private boolean isGooglePlayServiceAvailable(boolean isAllowResolveError) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int availableCode = googleApiAvailability.isGooglePlayServicesAvailable(mActivity);

        if (availableCode == ConnectionResult.SUCCESS) { // SUCCESS, SERVICE_MISSING, SERVICE_UPDATING, SERVICE_VERSION_UPDATE_REQUIRED, SERVICE_DISABLED, SERVICE_INVALID
            return true;
        } else if (googleApiAvailability.isUserResolvableError(availableCode)) {
            if (isAllowResolveError) {
                googleApiAvailability.getErrorDialog(mActivity, availableCode, PLAY_SERVICES_RESOLUTION_REQUEST, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        // User cancel the update dialog
                        mResultCodePublisher.onNext(new GGCheckModel(false));
                    }
                }).show();
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }
    // ==========================================================================================================================================


    // Check GPS permission Android 6 ===================================================================================================
    private static final int PERMISSION_REQUEST_LOCATION_CODE = 9101;

    private Observable<Boolean> mCheckLocationPermissionObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
        @Override
        public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED

                    || ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    ) {

                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_REQUEST_LOCATION_CODE);

                mResultCodePublisher.subscribe(result -> {
                    if (result instanceof PermissionCheckModel) {
                        e.onNext(((PermissionCheckModel) result).isSuccess());
                    }
                });

            } else {
                // Check permission done
                e.onNext(true);
            }
        }
    });


    // =================================================================================================================================


    // Check setting location on ===================================================================================================

    private static final int REQUEST_CHECK_SETTINGS = 9102;

    private Observable<Boolean> mCheckLocationServiceOnObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
        @Override
        public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
            checkTurnOnLocationService(e);

            mResultCodePublisher.subscribe(result -> {
                if (result instanceof LocationOnCheckModel) {
                    e.onNext(((LocationOnCheckModel) result).isSuccess());
                }
            });
        }
    });

    private void checkTurnOnLocationService(ObservableEmitter<Boolean> emitter) {
        // Create location setting request
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(mTimeRequestInterval);
        //mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setSmallestDisplacement(mMinDistanceRequest); // In meter
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(mActivity);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(mActivity, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
                mResultCodePublisher.onNext(new LocationOnCheckModel(true));
            }
        });

        task.addOnFailureListener(mActivity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // LocationModel settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(mActivity,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                        mResultCodePublisher.onNext(new LocationOnCheckModel(false));
                    }
                }
            }
        });
    }


    // Preference ==================================================
    private static final String PREF_NAME = "LocationTracker";
    private static final String KEY_LAT_LOCATION = "KEY_LAT_LOCATION";
    private static final String KEY_LON_LOCATION = "KEY_LAT_LOCATION";

    private SharedPreferences getSharePreference() {
        return mActivity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    private void saveLatestLocationFromPreference(Location location) {
        if (location == null) {
            return;
        }

        String latStr = String.valueOf(location.getLatitude());
        String lonStr = String.valueOf(location.getLongitude());

        getSharePreference().edit()
                .putString(KEY_LAT_LOCATION, latStr).apply();

        getSharePreference().edit()
                .putString(KEY_LON_LOCATION, lonStr).apply();
    }

    private Location getLatestLocationFromPreference() {
        String latStr = getSharePreference().getString(KEY_LAT_LOCATION, null);
        String lonStr = getSharePreference().getString(KEY_LON_LOCATION, null);

        if (latStr == null || lonStr == null) {
            return null;
        }

        Location location = new Location("");
        location.setLatitude(Double.valueOf(latStr));
        location.setLongitude(Double.valueOf(lonStr));

        return location;
    }

    // =======================================================================================================================================

    public void setTimeRequestInterval(long timeRequestInterval) {
        mTimeRequestInterval = timeRequestInterval;
    }

    public void setMinDistanceRequest(float minDistanceRequest) {
        mMinDistanceRequest = minDistanceRequest;
    }
}
