package com.eglife.base.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_ERROR = 0;

    @SerializedName("status")
    @Expose
    protected int status;

    @SerializedName("message")
    @Expose
    protected String message;

    public boolean isSuccess() {
        // If response don't have status field or status is SUCCESS -> Return success
        return status == STATUS_SUCCESS;
    }

    public boolean isError() {
        return !isSuccess();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
