package com.eglife.base.services.api;

public class ObserverResponse<T> {
    private int type;
    private T data;

    public ObserverResponse() {
    }

    public ObserverResponse(T data) {
        this.data = data;
    }

    public ObserverResponse(int type, T data) {
        this.type = type;
        this.data = data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
