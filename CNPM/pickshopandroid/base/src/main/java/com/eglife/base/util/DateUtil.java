package com.eglife.base.util;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    public static Date convertStringToDate(String strDate, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());

        Date d = null;
        try {
            d = sdf.parse(strDate);
        } catch (Exception e) {
            d = null;
        }
        return d;
    }

    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        String strDate = null;

        try {
            strDate = sdf.format(date);
        } catch (Exception e) {
            strDate = null;
        }
        return strDate;
    }

    public static String convertDateStringToAnotherFormat(String dateSt,
                                                          String oldFormat,
                                                          String newFormat) {
        SimpleDateFormat oldFormatSDF = new SimpleDateFormat(oldFormat, Locale.getDefault());
        Date date;
        try {
            date = oldFormatSDF.parse(dateSt);

            SimpleDateFormat newFormatSDF = new SimpleDateFormat(newFormat, Locale.getDefault());
            return newFormatSDF.format(date);
        } catch (ParseException e) {
            return dateSt;
        }
    }

    public static String getCurrentDate(String format) {
        Calendar c = Calendar.getInstance();
        return convertDateToString(c.getTime(), format);
    }

    public static String convertTime(long time, String formatTime){
        Date date = new Date(time);
        Format format = new SimpleDateFormat(formatTime);
        return format.format(date);
    }

    public static boolean afterCurrentTime(String timeStr, String format) {
        Calendar c = Calendar.getInstance();
        long current = c.getTimeInMillis();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(timeStr);

            long time = date.getTime();
            return current < time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * compare two timestrings with format
     * @param time1Str
     * @param time2Str
     * @param format
     * @return true if time2 > time1
     *         false if time2 <= time1
     */
    public static boolean compareTime(String time1Str, String time2Str, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date1 = sdf.parse(time1Str);
            long time1 = date1.getTime();

            Date date2 = sdf.parse(time2Str);
            long time2 = date2.getTime();

            return time1 < time2;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static long getLongTime(String timeStr, String format) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(timeStr);
            long time = date.getTime();
            return time;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
