package com.eglife.base.gui.loading;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.husvnu.base.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import static androidx.fragment.app.DialogFragment.STYLE_NO_TITLE;

// Call me
//ProgressExt progressExt = ProgressExt.newInstance();
//progressExt.show(getFragmentManager(), "");
//

public class CustomLoading extends DialogFragment {
    private ProgressBar mProgressBar;
    private boolean isDialogShown = false;
    
    public static CustomLoading newInstance() {
        CustomLoading dialog = new CustomLoading();
        dialog.setStyle(STYLE_NO_TITLE, R.style.Theme_Dialog_ProgressExt);
        dialog.setCancelable(false);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.progress_dialog_ext, container);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);

        isDialogShown = true;
    }

    @Override
    public void dismiss() {
        //super.dismiss();
        super.dismissAllowingStateLoss();
        isDialogShown = false;
    }

    public boolean isDialogShown() {
        return isDialogShown;
    }
}
