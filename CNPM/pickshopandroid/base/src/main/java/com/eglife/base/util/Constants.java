package com.eglife.base.util;

public class Constants {

    public final static String PREF_NAME = "tokyolife2018";

    // Network
    public static final int NETWORK_CONNECT_TIME_OUT = 30;
    public static final int NETWORK_READ_OUT = 30;


    //country code
    public final static String VN_CODE = "+84";

    public final static String SPACE = " ";
    public final static String COMMA = ",";
    public final static String BLANK = "";
    public static String COLON = ":";
    public static String HASH = "#";
    public static String DASH = "-";
    public static String UNDER_LINE = "_";
    public static String SLASH = "/";
    public static String BRACKET_OPEN = "(";
    public static String BRACKET_CLOSE = ")";

    public static final String DATE_DISPLAY_FORMAT = "dd/MM/yyyy";
    public static final String DATE_DATA_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_DATA_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_DISPLAY_FORMAT_1 = "dd-MM-yyyy";
    public static final String DATE_DISPLAY_FORMAT_2 = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_DISPLAY_FORMAT_3 = "HH:mm dd/MM/yyyy";
    public static final String DATE_DISPLAY_FORMAT_4 = "dd";
    public static final String DATE_DISPLAY_FORMAT_5 = "dd/MM";

}
