package com.eglife.base.services.location.model;

import android.location.Location;

public class LocationModelResult {
    public static final int ERROR_GG_PLAY_SERVICE_CANT_UPDATE = 1;
    public static final int ERROR_REQUEST_LOCATION_PERMISSION = 2;
    public static final int ERROR_TURN_ON_LOCATION = 3;

    private boolean hasError = false;
    private int errorCode;

    private Location location;

    public LocationModelResult() {
    }

    public LocationModelResult(Location location) {
        this.hasError = false;
        this.location = location;
    }

    public LocationModelResult(boolean hasError, int errorCode) {
        this.hasError = hasError;
        this.errorCode = errorCode;
        this.location = null;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
