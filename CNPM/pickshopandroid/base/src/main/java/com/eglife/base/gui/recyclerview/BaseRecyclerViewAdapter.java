package com.eglife.base.gui.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.husvnu.base.R;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private SwipeRefreshLayout mSwipeRefreshLayout;

    protected boolean isUseLoadMore = true;
    protected boolean isUseLoadMoreIndicator = true;
    protected int visibleThreshold = 10;

    private int mPageIndex = 1;
    protected int mPageSize = 10;
    private int totalResult;

    public interface IBaseRecyclerViewAdapter {
        void onPullToRefresh();
        void onLoadMore();
    }
    private IBaseRecyclerViewAdapter mListener;
    public void setListener(IBaseRecyclerViewAdapter listener) {
        mListener = listener;
    }

    private static final int VIEW_TYPE_LOADER = -1;

    protected Context mContext;
    protected List<T> mData = new ArrayList<>();

    private boolean isFetchingData = false;

    public BaseRecyclerViewAdapter(Context context, List<T> data,
                                   RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout) {
        mContext = context;
        setData(data);

        // Setup pull to refresh
        mSwipeRefreshLayout = swipeRefreshLayout;

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mListener != null) {
                        mListener.onPullToRefresh();
                    }
                }
            });
        }

        // Setup load more
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!isUseLoadMore) {
                    return;
                }

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if(!isFetchingData && totalItemCount <= (lastVisibleItem + visibleThreshold)
                        && mData != null && mData.size() < totalResult){
                    if(mListener != null){
                        mListener.onLoadMore();
                    }
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void startGetData(boolean isLoadMore) {
        isFetchingData = true;

        if (!isLoadMore) {
            resetPageIndex();
        } else {
            increasePageIndex();
        }

        if (isLoadMore && isUseLoadMoreIndicator) {
            notifyItemChanged(mData.size());
        }
    }

    public void getDataDone(boolean isPullToRefresh, boolean isLoadMore, List<T> data) {
        if (isPullToRefresh) {
            if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }

        if (!isLoadMore) {
            setData(data);
        } else {
            addData(data);
        }

        isFetchingData = false;
    }

    public void setData(List<T> data) {
        if (data != null) {
            mData.clear();
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addData(List<T> data) {
        if (data != null) {
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_load_more_item, parent, false);
            return new ViewHolderLoadingItem(itemView);
        } else {
            return onCreateViewHolderImpl(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BaseRecyclerViewAdapter.ViewHolderLoadingItem) {
            if (isFetchingData) {
                holder.itemView.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.setVisibility(View.GONE);
            }
        } else {
            onBindViewHolderImpl(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        // If no items are present, there's no need for loader
        if (mData == null || mData.size() == 0) {
            return 0;
        }

        if (isUseLoadMore && isUseLoadMoreIndicator && isFetchingData) {
            // +1 for loader
            return mData.size() + 1;
        } else {
            return mData.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isUseLoadMore && isUseLoadMoreIndicator && position != 0 && position == getItemCount() - 1 && isFetchingData) {
            return VIEW_TYPE_LOADER;
        } else {
            return getItemViewTypeImpl();
        }

    }

    private void resetPageIndex() {
        mPageIndex = 1;
    }

    private void increasePageIndex() {
        mPageIndex++;
    }

    public int getPageIndex() {
        return mPageIndex;
    }


    public int getPageSize() {
        return mPageSize;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }

    // Implement method ===============================================================
    protected int getItemViewTypeImpl() {return 0;}

    abstract protected RecyclerView.ViewHolder onCreateViewHolderImpl(@NonNull ViewGroup parent, int viewType);

    abstract protected void onBindViewHolderImpl(@NonNull RecyclerView.ViewHolder holder, int position);
    // ================================================================================

    public class ViewHolderLoadingItem extends RecyclerView.ViewHolder {
        public ViewHolderLoadingItem(View itemView) {
            super(itemView);
        }
    }
}
