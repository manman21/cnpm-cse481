package com.eglife.base.services.api;

public interface ApiCallBack {
    void onComplete(ApiResponse apiResponse);
}
