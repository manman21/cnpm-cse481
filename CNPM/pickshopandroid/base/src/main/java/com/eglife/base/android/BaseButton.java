package com.eglife.base.android;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class BaseButton extends AppCompatButton {
    public BaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
