package com.eglife.base.util;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    public static int convertStringToInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static double convertStringToDouble(String number) {
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return 0.0d;
        }
    }

    public static String convertIntToString(Integer number) {
        return number != null ? number.toString() : "0";
    }

    public static String formatCurrency(double number, String unit) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        try {
            return formatter.format(number) + unit;
        } catch (Exception e) {
            return "0";
        }
    }

    public static String[] convertArrayStringToArrayList(ArrayList<String> arrayList) {
        String[] arr = new String[arrayList.size()];
        arr = arrayList.toArray(arr);
        return arr;
    }

    public static String getFilePathFromUri(Context context, Uri uri) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor =  context.getContentResolver().query(uri, proj, null, null, null);
        if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidPhone(String phone)
    {
        String expression = "^[0-9]{10,11}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static String getShortcutLocationAddress(String locationAddress) {
        if (locationAddress != null && locationAddress.contains(",")) {
            String[] parts = locationAddress.split(",");
            return parts[0];
        } else {
            return locationAddress;
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
