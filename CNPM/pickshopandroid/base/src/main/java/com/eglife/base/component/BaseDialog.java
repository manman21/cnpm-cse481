package com.eglife.base.component;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
//import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;



public abstract class BaseDialog extends DialogFragment implements View.OnClickListener{
    protected String TAG = getClass().getSimpleName();
    protected BaseActivity mContext;
    protected Dialog mDialog;
    protected BaseDialogCallbackContract mBaseDialogCallbackContract;

    public void setBaseDialogCallbackContract(BaseDialogCallbackContract mBaseDialogCallbackContract) {
        this.mBaseDialogCallbackContract = mBaseDialogCallbackContract;
    }

    public String getDialogId() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container);
        mContext = (BaseActivity) getActivity();
        initDialog(view);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0.95f);
        dialog.getWindow().getAttributes().windowAnimations = getAnimation();
        mDialog = dialog;
        return dialog;
    }

    protected void hideKeyboard() {
        View view = mContext.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Defines screen content view from layout resource id.
     * @return the layout resource id
     */
    protected abstract int getLayoutResourceId();

    /**
     * initital screen
     */
    protected  abstract void initDialog(View view);

    /**
     * get animation show dialog
     * @return @style animation
     */
    protected abstract int getAnimation();

    public interface BaseDialogCallbackContract {
        <T> void OnDataReturn(int type, T data);
    }
}
