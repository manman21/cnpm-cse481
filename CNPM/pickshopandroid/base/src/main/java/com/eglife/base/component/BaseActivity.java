package com.eglife.base.component;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;

import com.eglife.base.gui.loading.CustomLoading;
import com.husvnu.base.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseActivity extends AppCompatActivity {

    public abstract int getLayoutId();
    public abstract int getToolbarId();

    protected abstract void initToolbar();
    protected abstract void initView(Bundle savedInstanceState);
    protected abstract void initData(Bundle savedInstanceState);
    protected abstract void onCreateFinish(Bundle savedInstanceState);

    protected Toolbar mToolbar;
    protected BaseMainFragment mCurrentMainFragment;
    protected int mOptionMenuId = 0;
    private CustomLoading mCustomLoading;
    private boolean mIsRunning;
    private boolean isTouchEnable = true;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    // Full screen layout
    final int flagsScreen = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    protected boolean isShowFullScreen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fullscreen
        if (isShowFullScreen) {
            final View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(flagsScreen);

            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                    {
                        decorView.setSystemUiVisibility(flagsScreen);
                    }
                }
            });

        }

        setContentView(getLayoutId());
        setupToolbar();
        initView(savedInstanceState);
        initData(savedInstanceState);
        onCreateFinish(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (isShowFullScreen) {
            getWindow().getDecorView().setSystemUiVisibility(flagsScreen);
        }
    }


    protected void setupToolbar() {
        if (getToolbarId() != -1) {
            mToolbar = findViewById(getToolbarId());
            if (mToolbar != null) {
                setSupportActionBar(mToolbar);
            }
            initToolbar();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mOptionMenuId != 0) {
            getMenuInflater().inflate(mOptionMenuId, menu);
        }

        return true;
    }

    public void startActivityWithAnimation(Intent intent, int enterAnim, int exitAnim){
        startActivity(intent);
        overridePendingTransition(enterAnim, exitAnim);
    }

    public void finishActivityWithAnimation(int enterAnim, int exitAnim){
        finish();
        overridePendingTransition(enterAnim, exitAnim);
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    public void setOptionMenuLayout(int menuId) {
        mOptionMenuId = menuId;
        invalidateOptionsMenu();
    }

    public boolean isRunning() {
        return mIsRunning;
    }

    public void setRunning(boolean running) {
        mIsRunning = running;
    }

    public void showLoading() {
        if (mCustomLoading == null) {
            mCustomLoading = CustomLoading.newInstance();
        }

        if (!mCustomLoading.isDialogShown()) {
            mCustomLoading.show(getSupportFragmentManager(), "");
        }
    }

    public void hideLoading() {
        if (mCustomLoading != null && mCustomLoading.isDialogShown()) {
            mCustomLoading.dismiss();
        }
    }

    public void showMessage(String title, String message, CommonCallBack callBack) {
        new AlertDialog.Builder(this, R.style.AppDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.dialog_confirm_close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (callBack != null) {
                            callBack.onAction("");
                        }
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void showDialogConfirm(String title, String message, String textPositive, String textNegative, CommonCallBack callBack) {
        new AlertDialog.Builder(this, R.style.AppDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(textPositive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (callBack != null) {
                            callBack.onAction("");
                        }
                    }
                })
                .setNegativeButton(textNegative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void showDialogConfirm(String title, String message, String textPositive, CommonCallBack callBack) {
        new AlertDialog.Builder(this, R.style.AppDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(textPositive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (callBack != null) {
                            callBack.onAction("");
                        }
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void showErrorMessage(String message) {
        showMessage("Thông báo", message, null);
    }

    public void showErrorMessage2(String message) {
        showMessage("Thiếu thông tin", message, null);
    }

    public void showErrorMessage(String message, CommonCallBack callBack) {
        showMessage("", message, callBack);
    }


    public void setCurrentMainFragment(BaseMainFragment currentMainFragment) {
        mCurrentMainFragment = currentMainFragment;
    }

    public void replaceFragment(int layout, BaseFragment fragment, boolean isAddToBackStack) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.replace(layout, fragment);

            if (isAddToBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }
    }

    public void replaceFragment(int layout, BaseFragment fragment, boolean isAddToBackStack, String tagFragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.replace(layout, fragment, tagFragment);

            if (isAddToBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }
    }

    public void replaceFragmentWithAnimation(int layout, BaseFragment fragment, boolean isAddToBackStack,
                                             String tagFragment, int animEnter, int animExit, int popEnter, int popExit) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.setCustomAnimations(animEnter, animExit, popEnter, popExit);
            fragmentTransaction.replace(layout, fragment, tagFragment);

            if (isAddToBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }
    }

    // Handle touch event ////////////////////////////////////////////////////////////
    public void enableTouchEvent() {
        isTouchEnable = true;
    }

    public void disableTouchEvent() {
        isTouchEnable = false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !isTouchEnable || super.dispatchTouchEvent(ev);
    }

    public void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    public <T> ObservableTransformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
