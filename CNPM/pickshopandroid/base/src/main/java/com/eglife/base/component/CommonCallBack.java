package com.eglife.base.component;

public interface CommonCallBack {
    void onAction(Object obj);
}
