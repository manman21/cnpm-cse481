package com.eglife.base.util;

import android.content.Context;
import android.widget.ImageView;

import com.eglife.base.libraries.glide.GlideApp;
import com.eglife.base.libraries.glide.GlideRequest;
import com.husvnu.base.R;
import com.squareup.picasso.Picasso;

public class ImageLoader {
    public static void loadImage(Context context, String url, ImageView imageView) {
        loadImage(context, url, imageView, false);
    }

    public static void loadImage(Context context, String url, ImageView imageView, Boolean isShowLoadingIndicator) {
        GlideRequest glideRequests =  GlideApp.with(context)
                .load(url);

        if (isShowLoadingIndicator) {
            glideRequests = glideRequests.placeholder(R.drawable.circle_loading);
        }

        glideRequests.into(imageView);

//        Picasso.with(context).load(url)
//                .placeholder(R.drawable.circle_loading_small)
//                .into(imageView);


    }
}
