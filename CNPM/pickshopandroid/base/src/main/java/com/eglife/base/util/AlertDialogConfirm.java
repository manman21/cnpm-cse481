package com.eglife.base.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.husvnu.base.R;


public class AlertDialogConfirm {
	public static void showGoToLinkConfirm(final Context context, final String message, final String link) {
		showDialogConfirm(context, message, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Open browser
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
				context.startActivity(i);
			}
		});
	}
	
	public static void showDialogConfirm(final Context context, String message, DialogInterface.OnClickListener okListener) {
		showDialogConfirm(context, context.getString(R.string.app_name), message, context.getString(R.string.dialog_btn_confirm), context.getString(R.string.dialog_btn_deny), okListener);
	}

	public static void showDialogRetry(final Context context, String message, DialogInterface.OnClickListener okListener) {
		showDialogConfirm(context, context.getString(R.string.app_name), message, "OK", "Retry", okListener);
	}
	
	public static void showDialogConfirm(final Context context, String title, String message, DialogInterface.OnClickListener okListener) {
		showDialogConfirm(context, title, message, "OK", "Cancel", okListener);
	}
	public static void showDialogConfirm(final Context context, String title, String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
		showDialogConfirm(context, title, message, "OK", "Cancel", okListener,cancelListener);
	}
	public static void showDialogConfirm(final Context context, String title, String message, String textOk, String textCancel, DialogInterface.OnClickListener okListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppDialogTheme)
				.setTitle(title)
				.setMessage(message)
				.setNegativeButton(textOk, okListener)
				.setPositiveButton(textCancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
		builder.setCancelable(false);
		builder.create().show();
	}
	public static void showDialogConfirm(final Context context, String title, String message, String textOk, String textCancel, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				context)
				.setTitle(title)
				.setMessage(message)
				.setNegativeButton(textOk, okListener)
				.setPositiveButton(textCancel,cancelListener);
		builder.setCancelable(false);
		builder.create().show();
	}
	
	public static void showDialogInfo(final Context context, final String message) {
		showDialogInfo(context, message, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
	}
	
	public static void showDialogInfo(final Context context, final String message, final DialogInterface.OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				context)
				.setTitle(R.string.app_name)
				.setMessage(message)
				.setPositiveButton("OK",listener);
		builder.setCancelable(false);
		builder.create().show();
	}

	public static void showDialogInfo(final Context context, final String title,
									   final String message, final DialogInterface.OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton("Đồng ý",listener);
		builder.setCancelable(false);
		builder.create().show();
	}

	public static void showMessageNetworking(final Context context, String title, String message, String textOk, DialogInterface.OnClickListener okListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppDialogTheme)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(textOk, okListener);
		builder.setCancelable(false);
		builder.create().show();
	}
}
