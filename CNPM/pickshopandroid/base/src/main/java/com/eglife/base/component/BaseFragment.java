package com.eglife.base.component;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {
    protected BaseActivity mContainerActivity;

    protected abstract int getLayoutResourceId();
    protected abstract void initView(View view);
    protected abstract void initData();
    protected abstract void onViewCreatedFinish();

    protected String getFragmentTag() {
        return BaseFragment.class.getSimpleName();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mContainerActivity = (BaseActivity) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Host activity isn't instance of BaseActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResourceId(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Load view done
        initView(view);

        // Init data
        initData();

        // View Created Finish
        onViewCreatedFinish();
    }
}
