package com.eglife.base.services.location.model;

public class GGCheckModel extends MessageModel {

    public GGCheckModel(boolean isErrorResolved) {
        this.isErrorResolved = isErrorResolved;
    }

    private boolean isErrorResolved;

    public boolean isErrorResolved() {
        return isErrorResolved;
    }
}
