package com.eglife.base.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapUtility {
    public static Marker createMarker(Context context, GoogleMap map, LatLng latLng, int iconResource, int width, int height) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getBitmapSizeIcon(context, width, height, iconResource)));

        return map.addMarker(markerOptions);
    }

    private static Bitmap getBitmapSizeIcon(Context context, int width, int height, int idIcon){
        BitmapDrawable bitmapDraw = (BitmapDrawable)context.getResources().getDrawable(idIcon);
        Bitmap b=bitmapDraw.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }

    public static LatLng convertFromLocationToLatLon(Location location) {
        return new LatLng(location.getLatitude(),location.getLongitude());
    }
}
