package com.eglife.base.services.location.model;

public class PermissionCheckModel extends MessageModel {

    public PermissionCheckModel(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    private boolean isSuccess;

    public boolean isSuccess() {
        return isSuccess;
    }
}
