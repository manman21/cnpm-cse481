package com.eglife.base.services.location.model;

public class LocationOnCheckModel extends MessageModel {

    public LocationOnCheckModel(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    private boolean isSuccess;

    public boolean isSuccess() {
        return isSuccess;
    }
}
