package com.eglife.base.component;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

public abstract class BaseMainFragment extends BaseFragment {
    protected abstract void setHeaderBarTitle();
    protected abstract void initHeaderBarMenuItem();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Reload toolbar
        setHeaderBarTitle();

        // Init option menu
        initHeaderBarMenuItem();

        // Set current fragment
        mContainerActivity.setCurrentMainFragment(this);
    }
}
