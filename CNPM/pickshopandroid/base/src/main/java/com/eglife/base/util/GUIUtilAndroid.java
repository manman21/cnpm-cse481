package com.eglife.base.util;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class GUIUtilAndroid {

    public static Point getScreenSize(Context mContext) {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        return new Point(width, height);
    }

    public static int getPixcelFromDip(Context mContext, float dipValue){
        Resources r = mContext.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dipValue,
                r.getDisplayMetrics()
        );

        return px;
    }

    public static int getDipFromPixel(Context mContext, float pixel){
        float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (pixel/scale);
    }

    /// Set include_header_menu margin =======================================================================
    public static void setLayoutMargin(View v, int left, int top, int right, int bottom){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        lp.setMargins(left, top, right, bottom);
    }

    public static void setLayoutMarginLeft(View v, int left){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        lp.setMargins(left, lp.topMargin, lp.rightMargin, lp.bottomMargin);
    }

    public static void setLayoutMarginTop(View v, int top){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        lp.setMargins(lp.leftMargin, top, lp.rightMargin, lp.bottomMargin);
    }

    public static void setLayoutMarginRight(View v, int right){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        lp.setMargins(lp.leftMargin, lp.topMargin, right, lp.bottomMargin);
    }

    public static void setLayoutMarginBottom(View v, int bottom){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        lp.setMargins(lp.leftMargin, lp.topMargin, lp.rightMargin, bottom);
    }

    public static void setLayoutMarginDip(Context mContext, View v, int left, int top, int right, int bottom){
        int leftInDp = getPixcelFromDip(mContext, left);
        int topInDp = getPixcelFromDip(mContext, top);
        int rightInDp = getPixcelFromDip(mContext, right);
        int bottomInDp = getPixcelFromDip(mContext, bottom);

        setLayoutMargin(v, leftInDp, topInDp, rightInDp, bottomInDp);
    }

    public static void setLayoutMarginDipLeft(Context mContext, View v, int left) {
        int leftInDp = getPixcelFromDip(mContext, left);
        setLayoutMarginLeft(v, leftInDp);
    }

    public static void setLayoutMarginDipTop(Context mContext, View v, int top) {
        int topInDp = getPixcelFromDip(mContext, top);
        setLayoutMarginLeft(v, topInDp);
    }

    public static void setLayoutMarginDipRight(Context mContext, View v, int right) {
        int rightInDp = getPixcelFromDip(mContext, right);
        setLayoutMarginLeft(v, rightInDp);
    }

    public static void setLayoutMarginDipBottom(Context mContext, View v, int bottom) {
        int bottomInDp = getPixcelFromDip(mContext, bottom);
        setLayoutMarginLeft(v, bottomInDp);
    }

    ///  =======================================================================

    public static void setWidth(Context mContext, View v, int width) {
        ViewGroup.LayoutParams params=v.getLayoutParams();
        params.width=width;
        v.setLayoutParams(params);
    }

    public static void setHeight(Context mContext, View v, int height) {
        ViewGroup.LayoutParams params=v.getLayoutParams();
        params.height=height;
        v.setLayoutParams(params);
    }
}
