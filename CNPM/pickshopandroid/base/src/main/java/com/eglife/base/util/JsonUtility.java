package com.eglife.base.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonUtility {

    public static String getStringValueFromJsonElement(JsonElement jsonElement, String key) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        return getStringValueFromJsonObject(jsonObject, key);
    }

    public static Integer getIntValueFromJsonElement(JsonElement jsonElement, String key) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        return getIntValueFromJsonObject(jsonObject, key);
    }

    public static String getStringValueFromJsonObject(JsonObject jsonObject, String key) {
        if (jsonObject == null) {
            return null;
        }

        try {
            if (jsonObject.has(key)) {
                return jsonObject.get(key).getAsString();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static Integer getIntValueFromJsonObject(JsonObject jsonObject, String key) {
        if (jsonObject == null) {
            return null;
        }

        try {
            if (jsonObject.has(key)) {
                return jsonObject.get(key).getAsInt();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
