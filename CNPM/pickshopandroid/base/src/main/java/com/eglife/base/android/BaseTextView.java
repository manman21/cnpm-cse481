package com.eglife.base.android;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

public class BaseTextView extends AppCompatTextView {
    public BaseTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
