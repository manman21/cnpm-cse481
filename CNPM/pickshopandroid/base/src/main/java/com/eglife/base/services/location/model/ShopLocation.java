package com.eglife.base.services.location.model;

public class ShopLocation {
    public double latitude;
    public double longitude;

    public ShopLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public ShopLocation() {
    }

    @Override
    public String toString() {
        return "ShopLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
