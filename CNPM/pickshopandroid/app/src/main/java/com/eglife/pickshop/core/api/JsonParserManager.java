package com.eglife.pickshop.core.api;

public class JsonParserManager {
    private static JsonParserManager instance;

    public static JsonParserManager getInstance() {
        if (instance == null) {
            instance = new JsonParserManager();
        }

        return instance;
    }
}
