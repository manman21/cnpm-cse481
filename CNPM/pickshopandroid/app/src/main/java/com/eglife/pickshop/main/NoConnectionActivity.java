package com.eglife.pickshop.main;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.eglife.base.component.BaseActivity;

import com.eglife.pickshop.R;
import com.eglife.pickshop.core.localstorate.PreferenceManager;
import com.eglife.pickshop.main.auth.LoginActivity;
import com.eglife.pickshop.main.createshop.CreateShopActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoConnectionActivity extends BaseActivity {

    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context, NoConnectionActivity.class);
        return intent;
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_no_connection;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    @OnClick(R.id.rl_retry)
    void btnRetryOnClick(){
       if(!isNetworkAvailable()){
           startActivity(NoConnectionActivity.getStartIntent(this));
       } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    goNextScreen();
                }
            }, 0);
        };
    }
    public void goNextScreen() {
        //PreferenceManager.getInstances().clearUserData();
        if (PreferenceManager.getInstances().isUserLogin()){
            Log.e("asd", PreferenceManager.getInstances().getUserId());
            startActivity(CreateShopActivity.getStartIntent(this, null));
        } else {
            startActivity(LoginActivity.getStartIntent(this));
        }

        finish();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
