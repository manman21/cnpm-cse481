package com.eglife.pickshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateShopResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private Shop shop;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public class Shop {
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("lat_location")
        @Expose
        private String latLocation;
        @SerializedName("long_location")
        @Expose
        private String longLocation;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatLocation() {
            return latLocation;
        }

        public void setLatLocation(String latLocation) {
            this.latLocation = latLocation;
        }

        public String getLongLocation() {
            return longLocation;
        }

        public void setLongLocation(String longLocation) {
            this.longLocation = longLocation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Shop{" +
                    "name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    ", latLocation='" + latLocation + '\'' +
                    ", longLocation='" + longLocation + '\'' +
                    ", description='" + description + '\'' +
                    ", updatedAt='" + updatedAt + '\'' +
                    ", createdAt='" + createdAt + '\'' +
                    ", id=" + id +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CreateShopResponse{" +
                "shop=" + shop +
                '}';
    }
}
