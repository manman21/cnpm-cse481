package com.eglife.pickshop.core.api;

import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.BaseRepository;
import com.eglife.pickshop.core.api.model.CreateShopResponse;
import com.eglife.pickshop.core.api.model.LoginResponse;

import retrofit2.Call;

public class Repository extends BaseRepository {
    public void login(String phone, String password, Integer type, ApiCallBack apiCallBack) {
        Call<LoginResponse> call = RetrofitManager.getService().login(phone, password, type);
        callApiWithDataResponse(call, apiCallBack);
    }

    public void createShop(String name, String address,
                            String latitude, String longitude, String description,
                            ApiCallBack apiCallBack) {

        Call<CreateShopResponse> call = RetrofitManager.getService().createShop(name, address,
                latitude, longitude, description);
        callApiWithDataResponse(call, apiCallBack);
    }
}
