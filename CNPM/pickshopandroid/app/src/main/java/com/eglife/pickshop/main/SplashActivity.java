package com.eglife.pickshop.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.eglife.pickshop.R;
import com.eglife.pickshop.core.localstorate.PreferenceManager;
import com.eglife.pickshop.main.auth.LoginActivity;
import com.eglife.pickshop.main.createshop.CreateShopActivity;

import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeScreen();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void changeScreen(){
        if(!isNetworkAvailable()){
            startActivity(NoConnectionActivity.getStartIntent(this));
        } else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    goNextScreen();
                }
            }, 0);
        }
    }

    public void goNextScreen() {
        //PreferenceManager.getInstances().clearUserData();
        if (PreferenceManager.getInstances().isUserLogin()){
            Log.e("asd", PreferenceManager.getInstances().getUserId());
            startActivity(CreateShopActivity.getStartIntent(this, null));
        } else {
            startActivity(LoginActivity.getStartIntent(this));
        }

        finish();
    }


}
