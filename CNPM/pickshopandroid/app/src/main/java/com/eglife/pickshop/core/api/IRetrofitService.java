package com.eglife.pickshop.core.api;

import com.eglife.pickshop.core.api.model.CreateShopResponse;
import com.eglife.pickshop.core.api.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface IRetrofitService {
    @FormUrlEncoded
    @POST("users/login")
    Call<LoginResponse> login(@Field("phone") String phone, @Field("password") String password, @Field("type") Integer type);

    @FormUrlEncoded
    @POST("shops/create-shop")
    Call<CreateShopResponse> createShop (@Field("name") String name,
                                         @Field("address") String address,
                                         @Field("lat_location") String latitude,
                                         @Field("long_location") String longitude,
                                         @Field("description") String description
    );
}
