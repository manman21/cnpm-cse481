package com.eglife.pickshop.main.home;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.eglife.base.component.BaseActivity;
import com.eglife.pickshop.R;
import com.eglife.pickshop.main.createshop.CreateShopActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static androidx.core.content.ContextCompat.startActivity;

public class ShopListActivity extends BaseActivity {
    @BindView(R.id.img_plus)
    View mBtnPlus;

    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context, ShopListActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_shop_list;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {

    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    @OnClick(R.id.img_plus)
    void buttonPlusOnClick() {
            startActivity(CreateShopActivity.getStartIntent(this, null));
    }
}
