package com.eglife.pickshop.main.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.eglife.base.component.BaseActivity;
import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.ApiResponse;
import com.eglife.pickshop.R;
import com.eglife.pickshop.core.api.Repository;
import com.eglife.pickshop.core.api.model.LoginResponse;
import com.eglife.pickshop.core.localstorate.PreferenceManager;
import com.eglife.pickshop.main.createshop.CreateShopActivity;
import com.eglife.pickshop.main.home.ShopListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_login_phone)
    EditText mInputPhone;

    @BindView(R.id.et_login_password)
    EditText mInputPassword;

    @BindView(R.id.tv_header)
    TextView mTvHeader;

    @BindView(R.id.tv_des)
    TextView mTvDescription;

    @BindView(R.id.tv_wrong_acc_infor)
    TextView mTvWrongInfor;

    public static Intent getStartIntent(Context mContext) {
        Intent intent = new Intent(mContext, LoginActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.acticity_login;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);

       /* mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goShopListScreen();
            }
        });*/
    }

    private void goShopListScreen() {
        startActivity(ShopListActivity.getStartIntent(this));
        finish();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    @OnClick(R.id.btn_login)
    void buttonLoginOnClick() {
        String phone = mInputPhone.getText().toString();
        //String phone = "0364114082";
        String password = mInputPassword.getText().toString();
        //String password = "123456";
        Integer type = 2;

        showLoading();
        new Repository().login(phone, password, type, new ApiCallBack() {
            @Override
            public void onComplete(ApiResponse apiResponse) {
                hideLoading();
                if (apiResponse.isSuccess()) {
                    LoginResponse.User user = ((LoginResponse)(apiResponse.getData())).getUser();

                    PreferenceManager.getInstances().setUserId(String.valueOf(user.getUserId()));
                    PreferenceManager.getInstances().setUserName(user.getName());
                    PreferenceManager.getInstances().setPrefPhone(phone);
                    PreferenceManager.getInstances().setUserPass(password);
                    PreferenceManager.getInstances().setEmail(user.getEmail());
                    PreferenceManager.getInstances().setUserToken(user.getRememberToken());

                    startActivity(CreateShopActivity.getStartIntent(LoginActivity.this, null));
                    finish();

                } else {
                    //showErrorMessage(apiResponse.getMessage());
                    mTvWrongInfor.setVisibility(View.VISIBLE);
                }

            }
        });
    }
}
