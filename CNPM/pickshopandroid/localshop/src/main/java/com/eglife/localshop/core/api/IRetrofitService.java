package com.eglife.localshop.core.api;

import com.eglife.localshop.core.api.model.CreateShopResponse;
import com.eglife.localshop.core.api.model.DeleteShopResponse;
import com.eglife.localshop.core.api.model.GetShopDetailResponse;
import com.eglife.localshop.core.api.model.GetShopResponse;
import com.eglife.localshop.core.api.model.LoginResponse;
import com.eglife.localshop.core.api.model.UpdateShopResponse;
import com.eglife.localshop.core.api.model.UploadResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface IRetrofitService {
    @FormUrlEncoded
    @POST("owners/login")
    Call<LoginResponse> login(@Field("phone") String phone, @Field("password") String password, @Field("type") Integer type);

    @FormUrlEncoded
    @POST("ownershops/delete-shop")
    Call<DeleteShopResponse> deleteShop(@Field("shop_id") Integer shopId);

    @FormUrlEncoded
    @POST("ownershops/create-shop")
    Call<CreateShopResponse> createShop(@Field("name") String name,
                                        @Field("address") String address,
                                        @Field("lat_location") String latitude,
                                        @Field("long_location") String longitude,
                                        @Field("description") String description,
                                        @Field("user_id") Integer userId,
                                        @Field("phone") String phone,
                                        @Field("image") String image
    );


    @GET("ownershops/get-owners-shop")
    Call<GetShopResponse> getShop(@Query("user_id") Integer userId);

    @GET("ownershops/get-detail-shop")
    Call<GetShopDetailResponse> getShopDetail(@Query("shop_id") Integer shopId);

    @FormUrlEncoded
    @POST("ownershops/update-shop")
    Call<UpdateShopResponse> updateShop(@Field("shop_id") Integer shopId,
                                        @Field("name") String name,
                                        @Field("address") String address,
                                        @Field("lat_location") String latitude,
                                        @Field("long_location") String longitude,
                                        @Field("description") String description,
                                        @Field("phone") String phone,
                                        @Field("image") String image
    );

    @Multipart
    @POST("ownershops/upload")
    Call<UploadResponse> upload(
            @Part("user_id") Integer userId,
            @Part MultipartBody.Part file
    );
}
