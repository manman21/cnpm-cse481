package com.eglife.localshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateShopResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private Update update;

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public class Update{
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("lat_location")
        @Expose
        private String latLocation;
        @SerializedName("long_location")
        @Expose
        private String longLocation;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("image")
        @Expose
        private String image;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatLocation() {
            return latLocation;
        }

        public void setLatLocation(String latLocation) {
            this.latLocation = latLocation;
        }

        public String getLongLocation() {
            return longLocation;
        }

        public void setLongLocation(String longLocation) {
            this.longLocation = longLocation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @Override
        public String toString() {
            return "Update{" +
                    "name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    ", latLocation='" + latLocation + '\'' +
                    ", longLocation='" + longLocation + '\'' +
                    ", description='" + description + '\'' +
                    ", phone='" + phone + '\'' +
                    ", image='" + image + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UpdateShopResponse{" +
                "update=" + update +
                '}';
    }
}
