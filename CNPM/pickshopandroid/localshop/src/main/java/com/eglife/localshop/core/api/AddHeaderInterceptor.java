package com.eglife.localshop.core.api;

import com.eglife.localshop.core.localstorate.PreferenceManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddHeaderInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request original = chain.request();

        //String defaultLanguage = Locale.getDefault().getLanguage();

        String token = PreferenceManager.getInstances().getUserToken();

        Request request = original.newBuilder()
                //.header("language", defaultLanguage)
                .header("token", token != null ? token : "")
                .method(original.method(), original.body())
                .build();

        return chain.proceed(request);
    }
}
