package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eglife.base.component.BaseActivity;
import com.eglife.base.component.CommonCallBack;
import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.ApiResponse;
import com.eglife.base.services.location.LocationTrackerManager;
import com.eglife.base.services.location.model.LocationModelResult;
import com.eglife.base.services.location.model.ShopLocation;
import com.eglife.base.util.LoggerManager;
import com.eglife.localshop.core.api.Repository;
import com.eglife.localshop.core.api.model.UploadResponse;
import com.eglife.localshop.core.localstorate.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CreateShopActivity extends BaseActivity {
    private static final String BUNDLE_SHOP_ID = "BUNDLE_SHOP_ID";
    public static final int PICK_IMAGE = 1;
    public static final int PICK_LOCATION = 2;
    private String imagePath;

    @BindView(R.id.tv_shop_infor)
    TextView mTextViewShopInfor;

    @BindView(R.id.tv_shop_location)
    TextView mTextViewShopLocation;

    @BindView(R.id.tv_shop_type)
    TextView mTextViewShopType;

    @BindView(R.id.tv_shop_name)
    TextView mTextViewShopName;

    @BindView(R.id.tv_short_des)
    TextView mTextViewShortDes;

    @BindView(R.id.tv_address)
    TextView mTextViewAddress;

    @BindView(R.id.tv_phone)
    TextView mTextViewPhone;

    @BindView(R.id.tv_shop_photo)
    TextView mTextViewShopPhoto;

    @BindView(R.id.tv_update)
    TextView mTextViewUpdate;

    @BindView(R.id.img_photo)
    ImageView mImageAddPhoto;

    @BindView(R.id.view_shop_location)
    EditText mTvShopLocation;

    @BindView(R.id.et_shop_name)
    EditText mEdtShopName;

    @BindView(R.id.et_short_des)
    EditText mEdtShortDes;

    @BindView(R.id.et_address)
    EditText mEdtAddress;

    @BindView(R.id.et_phone)
    EditText mEdtPhone;

    protected String lat, longt;

    private String mShopId = null;

    private LocationTrackerManager mLocationTrackerManager;

    public static Intent getStartIntent(Context context, String id) {
        Intent intent = new Intent(context, CreateShopActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_SHOP_ID, id);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_shop;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mShopId = bundle.getString(BUNDLE_SHOP_ID);
        }

        mLocationTrackerManager = new LocationTrackerManager(this);
        mLocationTrackerManager.setTimeRequestInterval(600000);
        //mLocationTrackerManager.setMinDistanceRequest(200);
        mLocationTrackerManager.setup();
    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {
        setupLocationTracker();
    }

    @OnClick(R.id.view_shop_location)
    void btnLocateOnclick() {
        String latlong = mTvShopLocation.getText().toString();
        String[] split = latlong.split(", ");
        String latitude = split[0];

        // Longitude
        String longitude = split[1];
        Log.e("asd", "latitude" + latitude + "longitude" + longitude);
        Double lat = Double.parseDouble(latitude);
        Double lng = Double.parseDouble(longitude);
        Intent mapIntent = new Intent(this, MapsActivity.class);
        mapIntent.putExtra("latitude", lat);
        mapIntent.putExtra("longitude", lng);
        startActivityForResult(mapIntent, PICK_LOCATION);
    }

    @OnClick(R.id.rl_tool_bar)
    void btnBackOnClick() {
        onBackPressed();
    }

    @OnClick(R.id.rl_bottom_bar)
    void btnUpdateOnclick() {
        // Token
        String token = PreferenceManager.getInstances().getUserToken();
        Log.e("abc", token);
        String getUserId = PreferenceManager.getInstances().getUserId();
        Integer userId = Integer.parseInt(getUserId);
        Integer relationship = 4;
        // Latitude
        String latlong = mTvShopLocation.getText().toString();
        String[] split = latlong.split(", ");
        String latitude = split[0];

        // Longitude
        String longitude = split[1];

        // Shop name
        String shopName = mEdtShopName.getText().toString();

        // Address
        String address = mEdtAddress.getText().toString();

        String phone = mEdtPhone.getText().toString();

        // Shop description
        String shortDes = mEdtShortDes.getText().toString();
        boolean checkLat = latitude.matches("[-+]?[0-9]*\\.?[0-9]+");
        boolean checkLon = longitude.matches("[-+]?[0-9]*\\.?[0-9]+");
        if (TextUtils.isEmpty(shopName) || TextUtils.isEmpty(latitude) || TextUtils.isEmpty(longitude) || !checkLat || !checkLon) {
            Log.e("asd", "shopId: " + mShopId + "userId: " + getUserId);
            Log.e("tag", "lat: " + latitude + " long: " + longitude + " checkLat:: " + checkLat + " checkLon: " + checkLon);
            showErrorMessage2("Bạn phải nhập đầy đủ thông tin trước khi cập nhật");
            return;
        }

        /*if (TextUtils.isEmpty(address)) {
            showErrorMessage("Địa chỉ không được để trống");
            return;
        }

        if (TextUtils.isEmpty(shortDes)) {
            showErrorMessage("Mô tả không được để trống");
            return;
        }*/

        if (mShopId == null) {
            // Create House
            showLoading();
            new Repository().createShop(shopName, address,
                    latitude, longitude, shortDes, userId, phone, imagePath,
                    new ApiCallBack() {
                        @Override
                        public void onComplete(ApiResponse apiResponse) {
                            hideLoading();
                            if (apiResponse.isSuccess()) {
                                showMessage("Thông báo", "Cập nhật cửa hàng thành công", new CommonCallBack() {
                                    @Override
                                    public void onAction(Object obj) {
                                        startActivity(ShopListActivity.getStartIntent(CreateShopActivity.this, null));
                                        finish();
                                    }
                                });
                                deleteAllContent();
                            } else {
                                showErrorMessage("Vui lòng kiểm tra lại kết nối Internet");
                            }
                        }
                    });
        }
    }

    private void setupLocationTracker() {
        Log.e("iii", "here");
        mLocationTrackerManager.getLocation().subscribe(locationResult -> {

            if (locationResult.getLocation() != null) {

                Location location = locationResult.getLocation();
                LoggerManager.debug("Location: " + location);
                ShopLocation shopLocation = new ShopLocation(location.getLatitude(), location.getLongitude());
                lat = String.valueOf(shopLocation.latitude);
                longt = String.valueOf(shopLocation.longitude);
                mTvShopLocation.setText(lat + ", " + longt);
                Log.e("iii", mTvShopLocation.toString());

            } else {
                if (locationResult.isHasError()) {
                    int errorCode = locationResult.getErrorCode();

                    switch (errorCode) {
                        case LocationModelResult.ERROR_GG_PLAY_SERVICE_CANT_UPDATE:
                            Log.e("iii", "GG Play can't update");
                            break;

                        case LocationModelResult.ERROR_REQUEST_LOCATION_PERMISSION:
                            Log.e("iii", "Request location permission fail");
                            break;

                        case LocationModelResult.ERROR_TURN_ON_LOCATION:
                            Log.e("iii", "Can't turn on LocationModel service");
                            break;

                        default:
                            break;
                    }
                }
            }
        });
    }

    public void deleteAllContent() {
        mTvShopLocation.clearComposingText();
        mEdtShopName.clearComposingText();
        mEdtAddress.clearComposingText();
        mEdtShortDes.clearComposingText();
    }

    @OnClick(R.id.img_photo)
    void btnUploadOnClick() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {
            Uri uri = data.getData();
            Log.e("asd", "uri: " + RealPathUtil.getRealPath(this, uri));
            String path = RealPathUtil.getRealPath(this, uri);
            uploadToServer(path);
        };
        if (requestCode == PICK_LOCATION) {
            if (resultCode == RESULT_OK) {
                Double lat = data.getDoubleExtra("latitude",0);
                Double lng = data.getDoubleExtra("longitude",0);
                mTvShopLocation.setText(lat + ", " + lng);
            }
        };
    }

    private void uploadToServer(String filePath) {
        String getUserId = PreferenceManager.getInstances().getUserId();
        Integer userId = Integer.parseInt(getUserId);
        File file = new File(filePath);
        RequestBody fileReqBody = RequestBody.create(file, MediaType.parse("image/*"));
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), fileReqBody);


        new Repository().upload(userId, part, new ApiCallBack() {
            @Override
            public void onComplete(ApiResponse apiResponse) {
                if (apiResponse.isSuccess()) {
                    UploadResponse.Upload upload = ((UploadResponse)(apiResponse.getData())).getUpload();
                    imagePath = upload.getPath();
                    Picasso.get().load(imagePath).fit().centerCrop().into(mImageAddPhoto);
                    Log.e("success", "success" + apiResponse);
                } else {
                    Log.e("failed", "failed" + apiResponse);
                }
            }
        });
    }


}
