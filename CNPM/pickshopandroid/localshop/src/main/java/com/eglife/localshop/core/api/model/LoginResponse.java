package com.eglife.localshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public class User {
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("remember_token")
        @Expose
        private String rememberToken;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }



        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }



        public String getRememberToken() {
            return rememberToken;
        }

        public void setRememberToken(String rememberToken) {
            this.rememberToken = rememberToken;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + userId +
                    ", phone='" + phone + '\'' +
                    ", type='" + type + '\'' +

                    ", name='" + name + '\'' +

                    ", rememberToken='" + rememberToken + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "user=" + user +
                '}';
    }
}
