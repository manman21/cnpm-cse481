package com.eglife.localshop.core.api;

import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.BaseRepository;
import com.eglife.localshop.core.api.model.CreateShopResponse;
import com.eglife.localshop.core.api.model.DeleteShopResponse;
import com.eglife.localshop.core.api.model.GetShopDetailResponse;
import com.eglife.localshop.core.api.model.GetShopResponse;
import com.eglife.localshop.core.api.model.LoginResponse;
import com.eglife.localshop.core.api.model.UpdateShopResponse;
import com.eglife.localshop.core.api.model.UploadResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;

public class Repository extends BaseRepository {
    public void login(String phone, String password, Integer type, ApiCallBack apiCallBack) {
        Call<LoginResponse> call = RetrofitManager.getService().login(phone, password, type);
        callApiWithDataResponse(call, apiCallBack);
    }

    public void createShop(String name, String address,
                            String latitude, String longitude, String description, Integer userId, String phone, String image,
                            ApiCallBack apiCallBack) {

        Call<CreateShopResponse> call = RetrofitManager.getService().createShop(name, address,
                latitude, longitude, description, userId, phone, image);
        callApiWithDataResponse(call, apiCallBack);
    }

    public void getShop(Integer userId,
                           ApiCallBack apiCallBack) {
        Call<GetShopResponse> call = RetrofitManager.getService().getShop(userId);
        callApiWithDataResponse(call, apiCallBack);
    }

    public void getShopDetail(Integer shopId,
                        ApiCallBack apiCallBack) {
        Call<GetShopDetailResponse> call = RetrofitManager.getService().getShopDetail(shopId);
        callApiWithDataResponse(call, apiCallBack);
    }
    public void updateShop(Integer shopId, String name, String address,
                            String latitude, String longitude, String description, String phone, String image,
                            ApiCallBack apiCallBack) {
        Call<UpdateShopResponse> call = RetrofitManager.getService().updateShop(shopId, name, address,
                latitude, longitude, description, phone, image);
        callApiWithDataResponse(call, apiCallBack);
    }
    public void upload(Integer userId, MultipartBody.Part file,
                        ApiCallBack apiCallBack) {
        Call<UploadResponse> call = RetrofitManager.getService().upload(userId,file);
        callApiWithDataResponse(call, apiCallBack);
    }
    public void deleteShop(Integer shopId,
                       ApiCallBack apiCallBack) {
        Call<DeleteShopResponse> call = RetrofitManager.getService().deleteShop(shopId);
        callApiWithDataResponse(call, apiCallBack);
    }
}
