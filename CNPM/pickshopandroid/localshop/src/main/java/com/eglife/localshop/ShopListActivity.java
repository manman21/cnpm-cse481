package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eglife.base.component.BaseActivity;
import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.ApiResponse;
import com.eglife.localshop.core.api.Repository;
import com.eglife.localshop.core.api.model.GetShopResponse;
import com.eglife.localshop.core.localstorate.PreferenceManager;
import com.eglife.localshop.model.ShopList;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShopListActivity extends BaseActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;

    @BindView(R.id.rc_shop)
    RecyclerView rcShop;


    public static Intent getStartIntent(Context context, String id) {
        Intent intent = new Intent(context, ShopListActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_shop_list;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {
        dl = (DrawerLayout) findViewById(R.id.activity_main);
        nv = (NavigationView) findViewById(R.id.nv);
        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);

        dl.addDrawerListener(t);
        t.syncState();

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.shop_list_menu:
                        startActivity(ShopListActivity.getStartIntent(ShopListActivity.this, null));
                        break;
                    case R.id.recruitment:
                        showErrorMessage("Tính năng dành cho gói nâng cao");
                        break;
                    case R.id.product:
                        showErrorMessage("Tính năng dành cho gói nâng cao");
                        break;
                    case R.id.delivery:
                        showErrorMessage("Tính năng dành cho gói nâng cao");
                        break;
                    default:
                        return true;
                }
                return true;

            }
        });
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        String getUserId = PreferenceManager.getInstances().getUserId();
        Integer userId = Integer.parseInt(getUserId);
        Log.e("userId", "UserId: " + userId);
        new Repository().getShop(userId, new ApiCallBack() {
            @Override
            public void onComplete(ApiResponse apiResponse) {
                if (apiResponse.isSuccess()) {
                    RecyclerViewAdapter mRcvAdapter;
                    ArrayList<ShopList> data;
                    data = new ArrayList<>();
                    ArrayList<GetShopResponse.OwnerShop> response = ((GetShopResponse) (apiResponse.getData())).getOwnerShop();
                    Log.e("response","response: "+response.size());


                    for (int i = 0; i < response.size(); i++) {
                        ShopList shopList = new ShopList();
                        shopList.setShopId(response.get(i).getId());
                        shopList.setShopName(response.get(i).getName());
                        shopList.setShopAddress(response.get(i).getAddress());
                        shopList.setShopDescription(response.get(i).getDescription());
                        data.add(shopList);
                    }
                    mRcvAdapter = new RecyclerViewAdapter(data);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rcShop.setLayoutManager(layoutManager);
                    rcShop.setAdapter(mRcvAdapter);
                } else {

                }
            }
        });
    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    @OnClick(R.id.btn_register)
    void btnRegisterOnClick() {
        startActivity(CreateShopActivity.getStartIntent(this, null));
    }

    @OnClick(R.id.iv_menu)
    void btnMenuOnClick() {
        dl = (DrawerLayout) findViewById(R.id.activity_main);
        dl.openDrawer(Gravity.START);
    }

    @OnClick(R.id.logout)
    void btnLogOutOnClick() {
        PreferenceManager.getInstances().clearUserData();
        startActivity(LoginActivity.getStartIntent(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
}
