package com.eglife.localshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private Upload upload;

    public Upload getUpload() {
        return upload;
    }

    public void setUpload(Upload upload) {
        this.upload = upload;
    }

    public class Upload{
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("path")
        @Expose
        private String path;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        @Override
        public String toString() {
            return "Upload{" +
                    "userId=" + userId +
                    ", path='" + path + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UploadResponse{" +
                "upload=" + upload +
                '}';
    }
}
