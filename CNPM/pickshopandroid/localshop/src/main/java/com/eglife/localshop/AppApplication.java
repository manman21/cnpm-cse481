package com.eglife.localshop;

import android.app.Application;

import com.eglife.localshop.core.localstorate.PreferenceManager;


public class AppApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Init preference manager
        PreferenceManager.initPreferenceManager(this);
    }
}
