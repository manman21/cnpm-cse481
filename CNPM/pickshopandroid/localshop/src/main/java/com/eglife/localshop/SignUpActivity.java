package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.eglife.base.component.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {

    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context, SignUpActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sign_up;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }
    @OnClick(R.id.tool_bar_back)
    void btnBackOnClick(){
        startActivity(LoginActivity.getStartIntent(this));
    }

    @OnClick(R.id.terms_of_service)
    void tvTermsOfServiceOnClick(){
        startActivity(TermsOfServiceActivity.getStartIntent(this));
    }
}

