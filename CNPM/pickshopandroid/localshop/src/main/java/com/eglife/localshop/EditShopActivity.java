package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eglife.base.component.BaseActivity;
import com.eglife.base.component.CommonCallBack;
import com.eglife.base.services.api.ApiCallBack;
import com.eglife.base.services.api.ApiResponse;
import com.eglife.localshop.core.api.Repository;
import com.eglife.localshop.core.api.model.GetShopDetailResponse;
import com.eglife.localshop.core.api.model.UploadResponse;
import com.eglife.localshop.core.localstorate.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditShopActivity extends BaseActivity {

    public static final int PICK_IMAGE = 1;
    public static final int PICK_LOCATION = 2;
    private String imagePath;

    @BindView(R.id.tv_shop_infor)
    TextView mTextViewShopInfor;

    @BindView(R.id.tv_shop_location)
    TextView mTextViewShopLocation;

    @BindView(R.id.tv_shop_type)
    TextView mTextViewShopType;

    @BindView(R.id.tv_shop_name)
    TextView mTextViewShopName;

    @BindView(R.id.tv_short_des)
    TextView mTextViewShortDes;

    @BindView(R.id.tv_address)
    TextView mTextViewAddress;

    @BindView(R.id.tv_phone)
    TextView mTextViewPhone;

    @BindView(R.id.tv_shop_photo)
    TextView mTextViewShopPhoto;

    @BindView(R.id.tv_update)
    TextView mTextViewUpdate;

    @BindView(R.id.img_photo)
    ImageView mImageAddPhoto;

    @BindView(R.id.view_shop_location)
    EditText mTvShopLocation;

    @BindView(R.id.et_shop_name)
    EditText mEdtShopName;

    @BindView(R.id.et_short_des)
    EditText mEdtShortDes;

    @BindView(R.id.et_address)
    EditText mEdtAddress;

    @BindView(R.id.et_phone)
    EditText mEdtPhone;

    @BindView(R.id.tv_delete_shop)
    TextView mDeleteShop;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EditShopActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_shop;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        mDeleteShop.setVisibility(View.VISIBLE);
        Integer shopId;
        shopId = Integer.parseInt(getIntent().getStringExtra("id"));
        Log.e("asdbd", "asdbd " + shopId);
        new Repository().getShopDetail(shopId, new ApiCallBack() {
            @Override
            public void onComplete(ApiResponse apiResponse) {
                if (apiResponse.isSuccess()) {
                    GetShopDetailResponse.Detail detail = ((GetShopDetailResponse) (apiResponse.getData())).getDetail();
                    mEdtShopName.setText(detail.getName());
                    mEdtAddress.setText(detail.getAddress());
                    mEdtShortDes.setText(detail.getDescription());
                    mTvShopLocation.setText(detail.getLatLocation() + ", " + detail.getLongLocation());
                    mEdtPhone.setText(detail.getPhone());
                    Picasso.get().load(detail.getImage()).fit().centerCrop().into(mImageAddPhoto);
                } else {
                    Log.e("api", "apiResponse: " + apiResponse);
                }
                ;
            }
        });
    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    @OnClick(R.id.rl_tool_bar)
    void btnBackOnClick() {
        onBackPressed();
    }

    @OnClick(R.id.rl_bottom_bar)
    void btnUpdateOnClick() {
        Integer shopId;
        shopId = Integer.parseInt(getIntent().getStringExtra("id"));
        String latlong = mTvShopLocation.getText().toString();
        String[] split = latlong.split(", ");
        String latitude = split[0];

        // Longitude
        String longitude = split[1];

        // Shop name
        String shopName = mEdtShopName.getText().toString();

        // Address
        String address = mEdtAddress.getText().toString();

        String phone = mEdtPhone.getText().toString();

        // Shop description
        String shortDes = mEdtShortDes.getText().toString();
        boolean checkLat = latitude.matches("[-+]?[0-9]*\\.?[0-9]+");
        boolean checkLon = longitude.matches("[-+]?[0-9]*\\.?[0-9]+");
        if (TextUtils.isEmpty(shopName) || TextUtils.isEmpty(latitude) || TextUtils.isEmpty(longitude) || !checkLat || !checkLon) {
            Log.e("tag", "lat: " + latitude + " long: " + longitude + " checkLat:: " + checkLat + " checkLon: " + checkLon);
            showErrorMessage2("Bạn phải nhập đầy đủ thông tin trước khi cập nhật");
            return;
        } else {
            showLoading();
            new Repository().updateShop(shopId, shopName, address,
                    latitude, longitude, shortDes, phone, imagePath, new ApiCallBack() {
                        @Override
                        public void onComplete(ApiResponse apiResponse) {
                            hideLoading();
                            if (apiResponse.isSuccess()) {
                                showMessage("Thông báo", "Cập nhật cửa hàng thành công", new CommonCallBack() {
                                    @Override
                                    public void onAction(Object obj) {
                                        startActivity(ShopListActivity.getStartIntent(EditShopActivity.this, null));
                                        finish();
                                    }
                                });
                            } else {
                                showErrorMessage("Vui lòng kiểm tra lại kết nối Internet");
                            }
                        }
                    });
        }
    }

    @OnClick(R.id.img_photo)
    void btnUploadOnClick() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {
            Uri uri = data.getData();
            Log.e("asd", "uri: " + RealPathUtil.getRealPath(this, uri));
            String path = RealPathUtil.getRealPath(this, uri);
            uploadToServer(path);
        };
        if (requestCode == PICK_LOCATION) {
            if (resultCode == RESULT_OK) {
                Double lat = data.getDoubleExtra("latitude",0);
                Double lng = data.getDoubleExtra("longitude",0);
                mTvShopLocation.setText(lat + ", " + lng);
            }
        };
    }

    private void uploadToServer(String filePath) {
        String getUserId = PreferenceManager.getInstances().getUserId();
        Integer userId = Integer.parseInt(getUserId);
        File file = new File(filePath);
        RequestBody fileReqBody = RequestBody.create(file, MediaType.parse("image/*"));
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), fileReqBody);


        new Repository().upload(userId, part, new ApiCallBack() {
            @Override
            public void onComplete(ApiResponse apiResponse) {
                if (apiResponse.isSuccess()) {
                    UploadResponse.Upload upload = ((UploadResponse) (apiResponse.getData())).getUpload();
                    imagePath = upload.getPath();
                    Picasso.get().load(imagePath).fit().centerCrop().into(mImageAddPhoto);
                    Log.e("success", "success" + apiResponse);
                } else {
                    Log.e("failed", "failed" + apiResponse);
                }
            }
        });
    }

    @OnClick(R.id.view_shop_location)
    void btnLocateOnclick() {

        String latlong = mTvShopLocation.getText().toString();
        String[] split = latlong.split(", ");
        String latitude = split[0];

        // Longitude
        String longitude = split[1];
        Log.e("asd", "latitude" + latitude + "longitude" + longitude);
        Double lat = Double.parseDouble(latitude);
        Double lng = Double.parseDouble(longitude);
        Intent mapIntent = new Intent(this, MapsActivity.class);
        mapIntent.putExtra("latitude", lat);
        mapIntent.putExtra("longitude", lng);
        startActivityForResult(mapIntent, PICK_LOCATION);
    }

    @OnClick(R.id.rl_delete_shop)
    void btnDeleteOnClick(){
        Integer shopId;
        shopId = Integer.parseInt(getIntent().getStringExtra("id"));
        showDialogConfirm("Thông báo", "Bạn có chắc muốn xóa cửa hàng này?", "Xác nhận", "Không", new CommonCallBack() {
            @Override
            public void onAction(Object obj) {
                new Repository().deleteShop(shopId, new ApiCallBack() {
                    @Override
                    public void onComplete(ApiResponse apiResponse) {
                        if(apiResponse.isSuccess()){
                            Log.e("clicked","click");
                            startActivity(ShopListActivity.getStartIntent(EditShopActivity.this,null));
                            finish();
                        } else {
                            Log.e("error","error: "+apiResponse);
                        }
                    }
                });
            }
        });
    }
}
