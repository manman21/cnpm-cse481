package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.eglife.base.component.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoConnectionActivity extends BaseActivity {

    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context, NoConnectionActivity.class);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_no_connection2;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateFinish(Bundle savedInstanceState) {

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @OnClick(R.id.rl_retry)
    void btnRetryOnClick(){
        if(!isNetworkAvailable()){
            Log.e("Test", "clicked");
            startActivity(NoConnectionActivity.getStartIntent(this));
        } else {
            startActivity(LoginActivity.getStartIntent(this));
        }
    }
}
