package com.eglife.localshop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private LatLng getPos;
    View mapView;
    private View locationButton;
    private Double lat;
    private Double lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        ButterKnife.bind(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        int height = 151;
        int width = 102;
        BitmapDrawable bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.marker);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        mMap = googleMap;
        Intent intent = getIntent();
        lat = intent.getDoubleExtra("latitude", 0);
        lng = intent.getDoubleExtra("longitude", 0);
        LatLng currentPos = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(currentPos).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
        mMap.setMinZoomPreference(18.0f);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPos));
        mMap.setMyLocationEnabled(true);
        //Disable Map Toolbar:
        mMap.getUiSettings().setMapToolbarEnabled(false);
        locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        locationButton.setVisibility(View.GONE);
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);rlp.setMargins(0,0,30,30);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                getPos = latLng;
            }
        });
    }

    @OnClick(R.id.tv_update)
    void btnUpdateOnClick() {
        Log.e("latt","lat:"+ getPos);
        Intent intent = new Intent();
        if(getPos != null){
            intent.putExtra("latitude", getPos.latitude);
            intent.putExtra("longitude", getPos.longitude);
        } else {
          intent.putExtra("latitude",lat);
          intent.putExtra("longitude",lng);
        };
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.ic_location)
    void btnGetLocationOnClick(){
        if(mMap!=null){
            if(locationButton!=null){
                locationButton.callOnClick();
            }
        }
//        int height = 151;
//        int width = 102;
//        BitmapDrawable bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.marker);
//        Bitmap b = bitmapdraw.getBitmap();
//        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
//        mMap.clear();
//        mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
    }

    @OnClick(R.id.tool_bar_back)
    void btnBackOnClick(){
        onBackPressed();
    }
}