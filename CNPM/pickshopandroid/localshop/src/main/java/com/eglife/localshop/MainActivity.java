package com.eglife.localshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.eglife.localshop.core.localstorate.PreferenceManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeScreen();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void changeScreen(){
        if(!isNetworkAvailable()){
            startActivity(NoConnectionActivity.getStartIntent(this));
        } else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    goNextScreen();
                }
            }, 0);
        }
    }

    public void goNextScreen() {
        //PreferenceManager.getInstances().clearUserData();
        if (PreferenceManager.getInstances().isUserLogin()){
            Log.e("asd", PreferenceManager.getInstances().getUserId());
            startActivity(ShopListActivity.getStartIntent(this, null));
        } else {
            startActivity(LoginActivity.getStartIntent(this));
        }

        finish();
    }
}
