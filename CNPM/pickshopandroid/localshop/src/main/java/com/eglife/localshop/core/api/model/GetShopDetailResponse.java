package com.eglife.localshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetShopDetailResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private Detail detail;

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public class Detail{
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("lat_location")
        @Expose
        private String latLocation;
        @SerializedName("long_location")
        @Expose
        private String longLocation;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("phone")
        @Expose
        private String phone;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatLocation() {
            return latLocation;
        }

        public void setLatLocation(String latLocation) {
            this.latLocation = latLocation;
        }

        public String getLongLocation() {
            return longLocation;
        }

        public void setLongLocation(String longLocation) {
            this.longLocation = longLocation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "Detail{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    ", latLocation='" + latLocation + '\'' +
                    ", longLocation='" + longLocation + '\'' +
                    ", description='" + description + '\'' +
                    ", image='" + image + '\'' +
                    ", phone='" + phone + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GetShopDetailResponse{" +
                "detail=" + detail +
                '}';
    }
}
