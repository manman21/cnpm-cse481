package com.eglife.localshop.core.api.model;

import com.eglife.base.services.api.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteShopResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private Delete delete;

    public Delete getDelete() {
        return delete;
    }

    public void setDelete(Delete delete) {
        this.delete = delete;
    }

    public class Delete{
        @SerializedName("shop_id")
        @Expose
        private Integer shopId;

        public Integer getShopId() {
            return shopId;
        }

        public void setShopId(Integer shopId) {
            this.shopId = shopId;
        }

        @Override
        public String toString() {
            return "Delete{" +
                    "shopId=" + shopId +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "DeleteShopResponse{" +
                "delete=" + delete +
                '}';
    }
}
