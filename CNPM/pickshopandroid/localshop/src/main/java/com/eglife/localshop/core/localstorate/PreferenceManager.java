package com.eglife.localshop.core.localstorate;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {
    private static final String APP_PREFERENCE_NAME = "pickShop";
    private static final String PREF_USER_ID = "user_id";
    private static final String PREF_USER_TOKEN = "user_token";
    private static final String PREF_USER_AVATAR = "user_avatar";
    private static final String PREF_EMAIL = "user_email";
    private static final String PREF_USER_PASS = "user_pass";
    private static final String PREF_USER_NAME = "user_name";
    private static final String PREF_FULL_NAME = "full_name";
    private static final String PREF_DISPLAY_NAME = "display_name";
    private static final String PREF_PHONE = "phone";

    private static PreferenceManager instances = null;
    private SharedPreferences sharedpreferences;

    public PreferenceManager(Context context) {
        this.sharedpreferences = context.getSharedPreferences(APP_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public static void initPreferenceManager(Context context) {
        if (instances == null) {
            instances = new PreferenceManager(context);
        }
    }

    public static synchronized PreferenceManager getInstances() {
        return instances;
    }


    // User id
    public void setUserId(String userId) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_USER_ID, userId);
        editor.commit();
    }

    public String getUserId() {
        return sharedpreferences.getString(PREF_USER_ID, null);
    }

    // User token
    public void setUserToken(String userToken) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_USER_TOKEN, userToken);
        editor.commit();
    }
    public String getUserToken() {
        return sharedpreferences.getString(PREF_USER_TOKEN, null);
    }

    // User email
    public void setEmail(String email) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_EMAIL, email);
        editor.commit();
    }
    public String getEmail() {
        return sharedpreferences.getString(PREF_EMAIL, null);
    }

    // User name
    public void setUserName(String userName) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }
    public String getUserName() {
        return sharedpreferences.getString(PREF_USER_NAME, null);
    }

    //User password
    public void setUserPass(String userPass) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_USER_PASS, userPass);
        editor.commit();
    }
    public String getUserPass() {
        return sharedpreferences.getString(PREF_USER_PASS, null);
    }

    // Clear user password
    public void clearUserNamePassword() {
        setEmail(null);
        setUserPass(null);
        setUserToken(null);
    }

    //Full name
    public void setFullName(String fullName) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_FULL_NAME, fullName);
        editor.commit();
    }
    public String getFullName() {
        return sharedpreferences.getString(PREF_FULL_NAME, null);
    }

    //Display name
    public void setDisplayName(String displayName) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_DISPLAY_NAME, displayName);
        editor.commit();
    }
    public String getDisplayName() {
        return sharedpreferences.getString(PREF_DISPLAY_NAME, null);
    }

    //Display avatar
    public void setPrefUserAvatar(String avatar) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_USER_AVATAR, avatar);
        editor.commit();
    }
    public String getPrefUserAvatar() {
        return sharedpreferences.getString(PREF_USER_AVATAR, null);
    }

    //Display phone
    public void setPrefPhone(String phone) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREF_PHONE, phone);
        editor.commit();
    }
    public String getPrefPhone() {
        return sharedpreferences.getString(PREF_PHONE, null);
    }

    public boolean isUserLogin() {
        String userId = getUserId();
        return userId != null;
    }

    public void clearUserData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(PREF_USER_ID);
        editor.remove(PREF_USER_TOKEN);
        editor.remove(PREF_USER_AVATAR);
        editor.remove(PREF_EMAIL);
        editor.remove(PREF_USER_NAME);
        editor.remove(PREF_USER_PASS);
        editor.remove(PREF_FULL_NAME);
        editor.remove(PREF_DISPLAY_NAME);
        editor.remove(PREF_PHONE);
        editor.commit();
    }
}
