package com.eglife.localshop;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eglife.localshop.model.ShopList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>{

    private ArrayList<ShopList> data;

    public RecyclerViewAdapter(ArrayList<ShopList> data) {
        this.data = data;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        ShopList shopList = data.get(position);

        Integer shopId = shopList.getShopId();
        String shopName = shopList.getShopName();
        String shopAddress = shopList.getShopAddress();
        String shopDescription = shopList.getShopDescription();

        holder.txtShopId.setText(Integer.toString(shopId));
        holder.txtShopName.setText(shopName);
        holder.txtShopAddress.setText(shopAddress);
        holder.txtShopDescription.setText(shopDescription);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rc_shop_id)
        TextView txtShopId;

        @BindView(R.id.rc_shop_name)
        TextView txtShopName;

        @BindView(R.id.rc_shop_address)
        TextView txtShopAddress;

        @BindView(R.id.rc_shop_description)
        TextView txtShopDescription;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            txtShopId = (TextView) itemView.findViewById(R.id.rc_shop_id);
            txtShopName = (TextView) itemView.findViewById(R.id.rc_shop_name);
            txtShopAddress = (TextView) itemView.findViewById(R.id.rc_shop_address);
            txtShopDescription = (TextView) itemView.findViewById(R.id.rc_shop_description);
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        void onClick(View view){
            Intent i=new Intent(view.getContext(), EditShopActivity.class);
            i.putExtra("id",txtShopId.getText().toString());
            view.getContext().startActivity(i);
        }

    }
}