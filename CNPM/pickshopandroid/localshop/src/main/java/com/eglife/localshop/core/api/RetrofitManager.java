package com.eglife.localshop.core.api;

import android.util.Log;

import androidx.annotation.NonNull;

import com.eglife.localshop.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager {
    public static String BASE_URL = "http://api.pick.com.vn/";
    //public static String BASE_URL = "http://localhost/api/";

    public static IRetrofitService getService() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.connectTimeout(180, TimeUnit.SECONDS); // connect timeout
        clientBuilder.readTimeout(180, TimeUnit.SECONDS); // socket timeout

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(@NonNull String message) {
                    Log.i("OkHttp", message);
                }
            });

            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(logging);
        }

        clientBuilder.addInterceptor(new AddHeaderInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build();

        return retrofit.create(IRetrofitService.class);

    }

}