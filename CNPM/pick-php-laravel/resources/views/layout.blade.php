<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">

    <title> @yield('title','Pick System') </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="">

    <!-- plugins css -->
    

    <!-- core css -->
    <link href="/libs/bootstrap-4.3.1/bootstrap.min.css" rel="stylesheet">


    @yield('css-define')

</head>

<body>

<!-- Layout START -->

<!-- Layout END -->


<!-- Content START -->
@yield('content')
<!-- Content END -->




<script src="{{ URL::asset('libs/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::asset('libs/OpenLayers-2.13.1/OpenLayers.js') }}"></script>
<script src="{{ URL::asset('libs/proj4js-combined.js') }}"></script>
<script src="{{ URL::asset('libs/bootstrap-4.3.1/bootstrap.min.js') }}"></script>

<script src="{{ URL::asset('libs/gasparesganga-jquery-loading-overlay/src/loadingoverlay.min.js') }}"></script>
<script src="{{ URL::asset('libs/gasparesganga-jquery-loading-overlay/extras/loadingoverlay_progress/loadingoverlay_progress.js') }}" ></script>

<script src="{{ URL::asset('js/common/common.js') }}"></script>
<script src="{{ URL::asset('js/common/apiHandler.js') }}"></script>

@yield('js-define')

</body>

</html>