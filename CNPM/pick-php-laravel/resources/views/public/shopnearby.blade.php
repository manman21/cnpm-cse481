@extends("layout")

@section('title','Shop near by')

@section('css-define')
    <link rel="stylesheet" href="{{ URL::asset('') }}">
@endsection

@section('content')
    <style type="text/css">
      html, body{
        width: 100%;
        height: 100%;
        margin: 0;
      }
      #streetMap{
        position: relative;
        width: 100%;
        height: 100%;
      }
      .scroll-box {
        float: left;
        height: 100vh;
        width: 100%;
        background: #F5F5F5;
        overflow-y: scroll;
       }
      #list-all{
        min-height: 450px;
      }
      div.sticky {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
      }
      .menu-btn{
        display: none;
      }
      .hidden{
        display: block;
      }
      @media only screen and (max-width: 1020px) {
        .hidden{
            display: none;
        }
        .menu-btn{
            display: block;
            position: relative;
            z-index: 999;
            width: 100%;
        }
        .container-menu{
            z-index: 999;
        }
        .container-map{
            display: contents;
        }
        #streetMap{
            position: absolute;
        }
      }

    </style>


<!-- Content Wrapper START -->
<div class="container-fluid">
    
    <div class="row">
        <!-- Menu left content -->
        <div class="menu-btn">
            <button class="show-menu-btn">Danh sách cửa hàng</button>
            <button class="close-menu-btn hidden">Close</button>
        </div>

        <!-- Left content START 1/3 screen -->
        
        <div class="col-sm-3 card container-menu hidden">
            
            
            {{-- <div class="input-group-lg mb-3 sticky">
                <input type="text" class="form-control" placeholder="Tìm kiếm">
                
            </div> --}}
            <div id="list-all" class="scroll-box">
              <ul id="list" class="results-list list-group list-group-flush">
                    
              </ul>
                
            </div>
            
        </div>
        
        <!-- Left content END -->

        <!-- Right content START 2/3 screen -->
        <div class="col-sm-9 container-map">
            {{-- <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="#" style="color:black;">White</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" style="color:grey;">Dark</a>
                </li>
            </ul> --}}
            
            <!-- Show your location at the map -->
            <div id="streetMap"></div>
            
            <!-- End show map -->

        </div>
        <!-- Right content END -->
    </div>

</div>
<!-- Content Wrapper END -->

@endsection


@section('js-define')
    <script src="{{ URL::asset('js/shopnearby.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZMNHYEdmzEnEXeHo3UEZMd9Gv6NnIAZY&callback=initMap"
    async defer></script>
    <script>
      $( '.menu-btn' ).click(function(){
          $('.show-menu-btn').toggleClass('hidden');
          $('.close-menu-btn').toggleClass('hidden');
      });
      $( '.show-menu-btn' ).click(function(){
          $('.container-menu').removeClass('hidden');
      });
      $( '.close-menu-btn' ).click(function(){
          $('.container-menu').addClass('hidden');
      });
    </script>


@endsection
