<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->comment('Cửa hàng bánh mỳ Ong Vàng');
            $table->string('address')->nullable()->comment('34/45 Ngõ Thông Phong, Tôn Đức Thắng');
            $table->string('lat_location')->nullable()->comment('Toa do cua hang lat');
            $table->string('long_location')->nullable()->comment('Toa do cua hang long');
            $table->text('description')->nullable()->comment('Bánh mỳ pate, ruốc, bánh mỳ chuột, bánh mỳ nóng giòn, bánh mỳ gối, bánh mỳ trứng chả');
            $table->string('phone')->nullable()->comment('0123456789');
            $table->string('image')->nullable()->comment('image url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('shops');
    }
}
