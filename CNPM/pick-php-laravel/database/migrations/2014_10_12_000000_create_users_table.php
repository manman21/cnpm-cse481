<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('phone')->nullable();
            $table->string('password');
            $table->integer('type')->nullable()->comment('1: admin, 2: editor , 3: customer, 4: owner');

            $table->string('username')->nullable()->comment('Jimmy');
            $table->string('name')->nullable()->comment('Tran Ngoc Hieu');
            $table->string('email')->nullable();

            $table->rememberToken()->nullable()->comment('chữ ký số được dùng để xác thực người dùng');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
