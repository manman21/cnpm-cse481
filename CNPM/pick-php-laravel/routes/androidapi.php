<?php

use App\Http\Controllers\OwnerShopsController;
use Illuminate\Http\Request;

Route::group(['namespace' => 'Auth'], function () {
    Route::post('owners/create-user', ['uses' => 'RegisterController@create']);
    Route::post('owners/login', ['uses' => 'LoginController@login']);
});
Route::group(['middleware' => ['authApi']], function () {
    Route::post('ownershops/create-shop', ['uses' => 'OwnerShopsController@create']);
    Route::post('ownershops/add-relation', ['uses' => 'OwnerShopsController@addRelation']);
    Route::get('ownershops/get-owners-shop', ['uses' => 'OwnerShopsController@getAllOwnersShop']);
    Route::get('ownershops/get-detail-shop', ['uses' => 'OwnerShopsController@detail']);
    Route::post('ownershops/update-shop', ['uses' => 'OwnerShopsController@update']);
    Route::post('ownershops/upload', ['uses' => 'OwnerShopsController@upload']);
    Route::post('ownershops/delete-shop', ['uses' => 'OwnerShopsController@delete']);
});
