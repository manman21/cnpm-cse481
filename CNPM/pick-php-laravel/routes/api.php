<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['cors']], function () {

	Route::get('public/shops-near-by', ['uses' => 'PublicController@getAllShopsNearBy']);

});


Route::group(['namespace' => 'Auth'], function(){

	// Các loại tài khoản 1:admin 2:editor 3:customer 4:owner

	// Đăng ký tài khoản type = 2 : editor 
	Route::post('users/create-user', ['uses' => 'RegisterController@create']);

	// Đăng nhập tài khoản type = 2 : editor 
	Route::post('users/login', ['uses' => 'LoginController@login']);

});

Route::group(['middleware' => ['authApi']], function(){

	// Tạo shop 
	Route::post('shops/create-shop', ['uses' => 'ShopsController@create']);

	// Cập nhật thông tin shop 
	Route::post('shops/update-shop', ['uses' => 'ShopsController@update']);

	// Xoá shop 
	Route::post('shops/delete-shop', ['uses' => 'ShopsController@delete']);

	// Danh sách tất cả các shop
	Route::get('shops/list-all-shops', ['uses' => 'ShopsController@index']);

	// Chi tiết thông tin shop
	Route::get('shops/detail-shop', ['uses' => 'ShopsController@detail']);
});
