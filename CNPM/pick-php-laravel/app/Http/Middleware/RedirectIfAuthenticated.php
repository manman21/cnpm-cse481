<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        

        return $next($request);
    }

    // public function handle($request, Closure $next)
    // {
    //     $remember_token = $request->header('remember_token');

    //     if (empty($remember_token)) {
    //         return "Token empty";
    //     }

    //     $user = User::where(["remember_token" => $remember_token])->first();

    //     if (empty($user)) {
    //         return "Token invalid";
    //     }

    //     $request->attributes->add(["user" => $user]);

    //     return $next($request);
    // }
}
