<?php

namespace App\Http\Middleware;

use App\Core\Utility\CommonUtility;
use App\User;
use Illuminate\Support\Facades\Response;
use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');

        // dd($request->headers->all());

        // return Response::json(["status" => 1, "data" => $token, "message" => "User unavailable"]);

        if (empty($token)) {
            return Response::json(["status" => 0, "data" => null, "message" => "Empty token 1"]);
        }

        $user = User::where(["remember_token" => $token])->first();

        if (empty($user)) {
            return Response::json(["status" => 0, "data" => null, "message" => "User unavailable"]);
        }

        $request->attributes->add(["user" => $user]);

        return $next($request);
    }
}
