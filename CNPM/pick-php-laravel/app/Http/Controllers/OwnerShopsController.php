<?php

namespace App\Http\Controllers;

use App\OwnerUserShop;
use App\Shop;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\UploadedFile;

class OwnerShopsController extends Controller
{
    public function create(Request $request)
    {

        $name = $request->input("name");
        $address = $request->input("address");
        $lat_location = $request->input("lat_location");
        $long_location = $request->input("long_location");
        $description = $request->input("description");
        $userId = $request->input("user_id");
        $phone = $request->input("phone");
        $image = $request->input("image");

        //create shop
        $ownerShop = new Shop();
        $ownerShop->name = $name;
        $ownerShop->address = $address;
        $ownerShop->lat_location = $lat_location;
        $ownerShop->long_location = $long_location;
        $ownerShop->description = $description;
        $ownerShop->phone = $phone;
        $ownerShop->image = $image;
        //$ownerShop->phone = $phone;

        $ownerShop->save();

        $shopId = Shop::where(["name" => $name])->pluck('id')->first();

        $ownerUserShop = new OwnerUserShop();
        $ownerUserShop->user_id = $userId;
        $ownerUserShop->shop_id = $shopId;
        $ownerUserShop->relationship = '2';
        $ownerUserShop->save();

        return Response::json(["status" => 1, "data" => $ownerShop, "message" => "Created"]);
    }

    public function getAllOwnersShop(Request $request)
    {
        $data = array();
        $userId = $request->input("user_id");
        $shops = OwnerUserShop::where(["user_id" => $userId])->pluck('shop_id');
        foreach($shops as $shop){
            $list = Shop::where(["id" => $shop])->first();
            array_push($data,$list);
        }
       

        return Response::json(["status" => 1, "data" => $data, "message" => "created"]);
    }

    public function update(Request $request)
    {

        $shop_id = $request->input("shop_id");
        $check_shop = Shop::where(["id" => $shop_id])->first();
        if (empty($check_shop)) {
            return Response::json(["status" => 0, "data" => null, "message" => "Shop empty"]);
        } else {
            $name = $request->input("name");
            $address = $request->input("address");
            $lat_location = $request->input("lat_location");
            $long_location = $request->input("long_location");
            $description = $request->input("description");
            $phone = $request->input("phone");
            $image = $request->input("image");

            $ownerShop = Shop::where(["id" => $shop_id])->update(
                array(
                    "name" => $name,
                    "address" => $address,
                    "lat_location" => $lat_location,
                    "long_location" => $long_location,
                    "description" => $description,
                    "phone" => $phone,
                    "image" => $image
                )
            );
        }

        return Response::json(["status" => 1, "data" => null, "message" => "Updated"]);
    }

    public function delete(Request $request)
    {

        $shop_id = $request->input("shop_id");
        $check_shop = Shop::where(["id" => $shop_id])->first();
        if (empty($check_shop)) {
            return Response::json(["status" => 0, "data" => null, "message" => "Shop unavailable"]);
        } else {
            $ownerUserShop = OwnerUserShop::where(["shop_id"=>$shop_id])->delete();
            $ownerShop = Shop::where(["id" => $shop_id])->delete();
        }

        return Response::json(["status" => 1, "data" => null, "message" => "Deleted"]);
    }

    public function detail(Request $request)
    {

        $shop_id = $request->input("shop_id");
        $check_shop = Shop::where(["id" => $shop_id])->first();
        if (empty($check_shop)) {
            return Response::json(["status" => 0, "data" => null, "message" => "Shop unavailable"]);
        } else {
            $ownerShop = Shop::where(["id" => $shop_id])->first();
        }

        return Response::json(["status" => 1, "data" => $ownerShop, "message" => "Shop detail"]);
    }

    public function upload(Request $request){
        $file = $request->file("image");
        $user_id = $request->input("user_id");
        if(!empty($file))
        {
            $imagePath = url("/images/upload/shop") . "/";
            // Save photo to disk
            $milliseconds = round(microtime(true) * 1000);
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $randName = substr(md5(microtime()),rand(0,26),5);
            $fileName = $milliseconds . "_" . $randName . ".$ext";
            $file->storeAs($user_id, $fileName, "upload_shop");
            // dd($file->storeAs($request_dhx_id, $fileName, "upload/shop"));
            $fileStorePath = $user_id . "/" . $fileName;
            // Get image size
            list($imageWidth, $imageHeight) = getimagesize(public_path("/images/upload/shop") . "/" . $fileStorePath);


            $data = ["user_id"=>$user_id, "path" => $imagePath . $fileStorePath,
                'image_width' => $imageWidth, 'image_height' => $imageHeight];
                return Response::json(["status" => 1, "data" => $data, "message" => "image uploaded"]);
        } else {
            return null;
        }
    }
}
