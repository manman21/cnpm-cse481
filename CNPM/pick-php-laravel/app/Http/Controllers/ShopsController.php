<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Closure;
use App\Shop;
use Illuminate\Support\Facades\Response;

class ShopsController extends Controller
{
    public function create(Request $request){
    	
    	$name = $request->input("name");
    	$address = $request->input("address");
    	$lat_location = $request->input("lat_location");
    	$long_location = $request->input("long_location");
    	$description = $request->input("description");

    	//create shop
        $shop = new Shop();
        $shop->name = $name;
        $shop->address = $address;
        $shop->lat_location = $lat_location;
        $shop->long_location = $long_location;
        $shop->description = $description;

        $shop->save();

    	return Response::json(["status" => 1, "data" => $shop, "message" => "Created"]);
    }

    public function update(Request $request){

    	$shop_id = $request->input("shop_id");
    	$check_shop = Shop::where(["id" => $shop_id])->first();
        if(empty($check_shop)){
            return Response::json(["status" => 0, "data" => null, "message" => "Shop empty"]);
        } else {
        	$name = $request->input("name");
	    	$address = $request->input("address");
	    	$lat_location = $request->input("lat_location");
	    	$long_location = $request->input("long_location");
	    	$description = $request->input("description");

        	$shop = Shop::where(["id" => $shop_id])->update(
        		array(
        			"name"=> $name ,
        			"address"=> $address ,
        			"lat_location"=> $lat_location ,
        			"long_location"=> $long_location ,
        			"description"=> $description
        		)
        	);
        }

    	return Response::json(["status" => 1, "data" => null, "message" => "Updated"]);
    }

    public function delete(Request $request){
    	
    	$shop_id = $request->input("shop_id");
    	$check_shop = Shop::where(["id" => $shop_id])->first();
        if(empty($check_shop)){
            return Response::json(["status" => 0, "data" => null, "message" => "Shop unavailable"]);
        } else {
        	$shop = Shop::where(["id" => $shop_id])->delete();
        }


    	return Response::json(["status" => 1, "data" => null, "message" => "Deleted"]);
    }

    public function index(){

    	

    	return "Index";
    }

    public function detail(Request $request){

    	$shop_id = $request->input("shop_id");
    	$check_shop = Shop::where(["id" => $shop_id])->first();
        if(empty($check_shop)){
            return Response::json(["status" => 0, "data" => null, "message" => "Shop unavailable"]);
        } else {
        	$shop = Shop::where(["id" => $shop_id])->first();
        }

    	return Response::json(["status" => 1, "data" => $shop, "message" => "Shop detail"]);
    }
}
