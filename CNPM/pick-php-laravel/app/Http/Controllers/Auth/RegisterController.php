<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'type' => ['required', 'integer'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $phone = $request->input("phone");
        $password = $request->input("password");
        $type = $request->input("type");
        $username = $request->input("username");
        $name = $request->input("name");
        $email = $request->input("email");

        $check_user = User::where(["phone" => $phone, "type"=> $type])->first();
        if(!empty($check_user)){
            return "Not null";
        }

        //create user
        $user = new User();
        $user->phone = $phone;
        $user->password = Hash::make($password);
        $user->type = $type; // 1:admin 2:editor 3:customer 4:owner
        $user->username = $username;
        $user->name = $name;
        $user->email = $email;
        
        $remember_token = Hash::make($user->id . Str::random(32));
        $user->remember_token = $remember_token;

        $user->save();

        return "Success";
    }
}
