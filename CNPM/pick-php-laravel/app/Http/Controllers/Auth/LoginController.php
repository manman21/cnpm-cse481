<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        
        $phone = $request->input("phone");
        $password = $request->input("password");
        $type = $request->input("type");

        $user = User::where(["phone" => $phone,"type" => $type])->first();
        if (empty($user)) {
            return Response::json(["status" => 0, "data" => null, "message" => "User unavailable"]);
        }

        if (Hash::check($password, $user->password)) {
            $remember_token = Hash::make($user->id . Str::random(32));
            $user->remember_token = $remember_token;
            $user->save();
        }

        $data = [
            "user_id" => $user->id,
            "phone" => $user->phone,
            "type" => "Editor",
            "username" => $user->username,
            "name" => $user->name,
            "email" => $user->email,
            "remember_token" => $remember_token
        ];

        return Response::json(["status" => 1, "data" => $data, "message" => "Login success"]);
    }
}
