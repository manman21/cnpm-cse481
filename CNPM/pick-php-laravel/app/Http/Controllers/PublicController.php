<?php

namespace App\Http\Controllers;
use App\Shop;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function getAllShopsNearBy() {
    	$shops = Shop::all();
    	return $shops;
    }
}
