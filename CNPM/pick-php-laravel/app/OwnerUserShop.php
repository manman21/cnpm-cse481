<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerUserShop extends Model
{
    //
    protected $table = 'owner_users_shops';

    public $timestamps = true;
}
