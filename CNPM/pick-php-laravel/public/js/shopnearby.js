var $div;
var $list;

// Init document
$(document).ready(function() {
    initVariable();
    getData();
    
});

function initVariable() {
	$div = $("#list-all");
    $list = $("#list");
}

function getData() {

	// api url : http://api.pick.com.vn
	$.ajax({
		method: "GET",
        url: "http://api.pick.com.vn/public/shops-near-by",
    }).done(function(response) {

        showData(response);
        
    }).fail(function() {

    });
}

function showData(data) {

	// console.log(data);

	var leng = data.length;

	for (var i = 0; i < leng; i++) {

		$div.append(
			'<li class="list-group-item search_results_entry">'+ 
            '<a href="#" onclick="init(' + data[i].lat_location + ',' + data[i].long_location + ')">' + 
            data[i].name + '. ' + data[i].address + '. </br>' + data[i].description +'</a>'+'</li>'
		);
	}
    init(21.0245876,105.8191061);
}

function init(lat,lon){

    // Openstreetmap start
	// console.log("Init streetMap");
	// epsg4326 = new OpenLayers.Projection("EPSG:4326")

	// // Clear html
	// $("#streetMap").html("");

	// // Init map
 //    map = new OpenLayers.Map({
 //    	div : streetMap,
 //    	displayProjection: epsg4326
 //    });

 //    // Add layer
 //    // map.addLayer(new OpenLayers.Layer.OSM());
 //    map.addLayer(new OpenLayers.Layer.OSM(
 //    	"Hydda",
 //      	["https://a.tile.openstreetmap.se/hydda/full/${z}/${x}/${y}.png"],
      
 //      {
 //        "tileOptions": 
 //        	{ 
 //        		"crossOriginKeyword": null 
 //        	}
 //      })
 //    );

 //    // Add current position
 //    // var lat = 20.9593029;
 //    // var lon = 105.7468539;
 //    var lonLat = new OpenLayers.LonLat( lon , lat )
 //          .transform(
 //            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
 //            map.getProjectionObject() // to Spherical Mercator Projection
 //          );
 //    var zoom=18;
 //    if (!map.getCenter()) {
 //      map.setCenter(lonLat, zoom);
 //    }


 //    // Add maker
 //    var markers = new OpenLayers.Layer.Markers( "Markers" );
 //    map.addLayer(markers);
 //    markers.addMarker(new OpenLayers.Marker(lonLat));
    // Openstreetmap end
    

    // Clear html
    $("#streetMap").html("");
    $('.show-menu-btn').removeClass('hidden');
    $('.container-menu').addClass('hidden');
    $('.close-menu-btn').addClass('hidden');

    // Init map
    var myLatlng = new google.maps.LatLng(lat,lon);
    var mapOptions = {
        zoom: 18,
        center: myLatlng,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false,
        fullscreenControl: false
    }
      
    var map = new google.maps.Map(document.getElementById("streetMap"), mapOptions);
      
    var marker = new google.maps.Marker({
        position: myLatlng,
    });
      
    marker.setMap(map);  
    
}